﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class Administration : HelpFunctions
    {
        public static void CreateUser(IWebDriver instance, Dictionary<string, string> dataArray)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            string password = ConfigurationManager.AppSettings["password"];

            LogAction("Verify that we are on the my businesses page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='wrapper']/div[1]/div/div/div[1]/h3"), 20).Text, Does.Contain("My Businesses"));

            string tableXPath = "//*[@id='businesses-table']";

            TableColumns.MyBusinesses(instance);

            LeftNavigation.TestTableVisibility(instance, tableXPath);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            try
            {
                LogAction("Click on the user management option");
                LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the gmb reporting option in the sidebar with JS");
                webElement2 = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            tableXPath = "//*[@id='users-table']";

            HelpFunctions.LogAction("Click on the pagination");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='users-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[4]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);

            LeftNavigation.TestTableVisibility(instance, tableXPath);

            LogAction("Click on the create user button");
            LocateElement(instance, By.XPath("//*[@id='create_user_button']/a"), 10).Click();

            Assert.That(LocateElement(instance, By.XPath("//*[@id='page_content_inner']/div/div/div[1]/h3"), 20).Text, Does.Contain("Create New Profile"));

            LogAction("Input email");
            LocateElement(instance, By.Id("email"), 10).Click();
            LocateElement(instance, By.Id("email"), 10).SendKeys(dataArray["email"]);

            LogAction("Input password");
            LocateElement(instance, By.Id("password"), 10).Click();
            LocateElement(instance, By.Id("password"), 10).Clear();
            LocateElement(instance, By.Id("password"), 10).SendKeys(password);

            LogAction("Input password for confirmation");
            LocateElement(instance, By.Id("password_repeat"), 10).Clear();
            LocateElement(instance, By.Id("password_repeat"), 10).SendKeys(password);

            LogAction("Input first name field");
            LocateElement(instance, By.Id("first_name"), 10).Click();
            LocateElement(instance, By.Id("first_name"), 10).SendKeys(dataArray["fname"]);

            LogAction("Input last name field blank");
            LocateElement(instance, By.Id("last_name"), 10).Clear();
            LocateElement(instance, By.Id("last_name"), 10).SendKeys(dataArray["lname"]);

            LogAction("Click on the create user button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/div/div/div[2]/form/div[7]/div/button"), 10).Click();

            LogAction("Assert were on the right page - contact info");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_edit_tabs_content']/li[1]/div/h3"), 20).Text, Does.Contain("Contact info"));

            LogAction("Click on roles and permissions tab");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[4]/a"), 10).Click();

            LogAction("Click on the selected role");
            LocateElement(instance, By.XPath(dataArray["xpath"]), 10).Click();

            LogAction("Click on the selectize");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[2]/div/div/div/div/div[3]/div/div/div[1]"), 10).Click();

            LogAction("Clear listing selectize");
            instance.ExecuteJavaScript("$('#type_id')[0].selectize.clear();");

            LogAction("Write luau in the field");
            LocateElement(instance, By.Id("type_id-selectized"), 10).SendKeys(dataArray["type"]);

            LogAction("Select the suggested hotel");
            LocateElement(instance, By.Id("type_id-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Save changes to the profile");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

        }

        public static void MakeTechnician(IWebDriver instance)
        {
            LogAction("Go back to homepage");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/a[3]"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            string tableXPath = "//*[@id='businesses-table']";

            TableColumns.MyBusinesses(instance);

            LeftNavigation.TestTableVisibility(instance, tableXPath);

            LogAction("Fitler table by luau");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("Luau");

            LogAction("Click on the result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Maximize browser window");
            instance.Manage().Window.Maximize();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            Thread.Sleep(1500);

            LogAction("Click on the administration tab");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            Thread.Sleep(2000);

            try
            {
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"), 10).SendKeys("operat");
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"), 10).SendKeys(Keys.Enter);
            }
            catch
            {
                LogAction("Click on the technician selectize");
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/div"), 10).Click();

                LogAction("Select suggested technician");
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"), 10).SendKeys(Keys.Backspace);

                LogAction("Input name");
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"), 10).SendKeys("operat");

                LogAction("Select suggested technician");
                LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"), 10).SendKeys(Keys.Enter);
            }
            
            LogAction("Save Listing");
            LocateElement(instance, By.XPath("//*[@id='listing_administration_save']"), 10).Click();

            Thread.Sleep(5000);
        }

        public static void GiveAccess(IWebDriver instance, bool isMultiAcc)
        {
            LogAction("Click on edit button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click on business access tab");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[2]/a"), 10).Click();

            LogAction("Click on the selectize");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs_content']/li[2]/div[2]/div/div/div[1]"), 10).Click();
            
            LogAction("Write luau in the field");
            LocateElement(instance, By.Id("listing-selectized"), 10).SendKeys("Luau");

            LogAction("Select the suggested hotel");
            LocateElement(instance, By.Id("listing-selectized"), 10).SendKeys(Keys.Enter);

            Thread.Sleep(1000);

            LogAction("Click on the give access button");
            LocateElement(instance, By.XPath("//*[@id='grantUserAccessButton']"), 10).Click();

            Thread.Sleep(5000);

            if (isMultiAcc == true)
            {
                LogAction("Write neptun in the field");
                LocateElement(instance, By.Id("listing-selectized"), 10).SendKeys("Нептун");

                LogAction("Select the suggested hotel");
                LocateElement(instance, By.Id("listing-selectized"), 10).SendKeys(Keys.Enter);

                Thread.Sleep(1000);

                LogAction("Click on the give access button");
                LocateElement(instance, By.XPath("//*[@id='grantUserAccessButton']"), 10).Click();

                Thread.Sleep(5000);
            }
        }

        public static void DeleteUser(IWebDriver instance, string emailToDelete)
        {
            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            try
            {
                LogAction("Click on the user management option");
                LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the gmb reporting option in the sidebar with JS");
                webElement2 = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            string tableXPath = "//*[@id='users-table']";

            HelpFunctions.LogAction("Click on the pagination");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='users-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[4]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);

            LeftNavigation.TestTableVisibility(instance, tableXPath);

            LogAction("Filter the table by email");
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).Click();
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).SendKeys(emailToDelete);

            LogAction("Click on the result to go to its profile");
            LocateElement(instance, By.XPath("//*[@id='users-table']/tbody/tr[1]/td[10]/div/div/a[1]"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Maximize browser window");
            instance.Manage().Window.Maximize();

            LogAction("Go to edit mode");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click deactivate account");
            LocateElement(instance, By.XPath("//*[@id='deleteUser']"), 10).Click();

            Thread.Sleep(1000);

            try
            {
                LogAction("Confirm deactivation");
                LocateElement(instance, By.XPath("//button[contains(.,'Yes, Delete')]"), 10).Click();
            }
            catch
            {
                LogAction("Click on the deactivate button with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='deleteUser']"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the confirmation button with JS");
                webElement2 = LocateElement(instance, By.XPath("//button[contains(.,'Yes, Delete')]"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);
            }
            
            try
            {
                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the gmb reporting option in the sidebar with JS");
                webElement2 = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            LogAction("Filter the table by email");
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).Click();
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).SendKeys(emailToDelete);

            Assert.That(LocateElement(instance, By.XPath("//*[@id='users-table_info']"), 20).Text, Does.Contain("Showing 0 to 0 of 0 entries"));
        }

        public static void DeleteOwnAcc(IWebDriver instance)
        {
            int hotelsCount = Int32.Parse(ConfigurationManager.AppSettings["multiHotelCount"]);

            LogAction("Wait for the loader to disappear");
            WaitForLoader(instance);

            LogAction("Make sure we are on the review management page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div/div/div/div[1]/h3"), 10).Text, Does.Contain("Review Management"));

            LogAction("Check that the number of hotels in the table is right");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='active-table_info']"), 20).Text, Does.Contain("Showing 1 to " + hotelsCount + " of 2 entries"));

            LogAction("Click on the profile picture icon");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10).Click();

            try
            {
                LogAction("Click on my profile");
                LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10).Click();
            }
            catch
            {
                LogAction("Click on my profile 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Click on edit button to go in edit mode");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click deactivate account");
            LocateElement(instance, By.XPath("//*[@id='deleteUser']"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Confirm deactivation");
            LocateElement(instance, By.XPath("//button[contains(.,'Yes, Delete')]"), 10).Click();
        }
    }
}
