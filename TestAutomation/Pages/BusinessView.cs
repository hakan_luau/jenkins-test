﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class BusinessView : HelpFunctions
    {
        public static Dictionary<string, string[]> administrationArray = new Dictionary<string, string[]>{
            { "technician", new string[] {
                "jaklin",
                "//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]/input"
            } },
            { "tier", new string[] {
                "4",
                "//*[@id='administration_form']/div[1]/div[4]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[1]/div[4]/div/div/div[1]/input"
            } },
            { "campaign", new string[] {
                "monthly",
                "//*[@id='administration_form']/div[2]/div[1]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[2]/div[1]/div/div/div[1]/input"
            } },
            { "businessStatus", new string[] {
                "new ope",
                "//*[@id='administration_form']/div[2]/div[2]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[2]/div[2]/div/div/div[1]/input"
            } },
            { "intStatus", new string[] {
                "in prog",
                "//*[@id='administration_form']/div[2]/div[3]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[2]/div[3]/div/div/div[1]/input"
            } },
            { "workStatus", new string[] {
                "active",
                "//*[@id='administration_form']/div[2]/div[4]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[2]/div[4]/div/div/div[1]/input"
            } },
            { "compStatus", new string[] {
                "non",
                "//*[@id='administration_form']/div[2]/div[5]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[2]/div[5]/div/div/div[1]/input"
            } },
            { "optStatus", new string[] {
                "acceler",
                "//*[@id='administration_form']/div[3]/div[1]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[3]/div[1]/div/div/div[1]/input"
            } },
            { "propertyType", new string[] {
                "hotel",
                "//*[@id='administration_form']/div[1]/div[4]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[1]/div[4]/div/div/div[1]/input"
            } },
            { "brand", new string[] {
                "doubletree",
                "//*[@id='administration_form']/div[3]/div[3]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[3]/div[3]/div/div/div[1]/input"
            } },
            { "paidStatus", new string[] {
                "paid",
                "//*[@id='administration_form']/div[3]/div[4]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[3]/div[4]/div/div/div[1]/input"
            } },
            { "region", new string[] {
                "europe",
                "//*[@id='administration_form']/div[3]/div[5]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[3]/div[5]/div/div/div[1]/input"
            } },
            { "subregion", new string[] {
                "europe",
                "//*[@id='administration_form']/div[4]/div[1]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[4]/div[1]/div/div/div[1]/input"
            } },
            { "director", new string[] {
                "oliver",
                "//*[@id='administration_form']/div[4]/div[2]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[4]/div[2]/div/div/div[1]/input"
            } },
            { "manager", new string[] {
                "luke",
                "//*[@id='administration_form']/div[4]/div[3]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[4]/div[3]/div/div/div[1]/input"
            } },
            { "seniorManager", new string[] {
                "aleksand",
                "//*[@id='administration_form']/div[4]/div[4]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[4]/div[4]/div/div/div[1]/input"
            } },
            { "team", new string[] {
                "europe",
                "//*[@id='administration_form']/div[4]/div[5]/div/div/div[1]/div",
                "//*[@id='administration_form']/div[4]/div[5]/div/div/div[1]/input"
            } },
        };

        public static Dictionary<string, string[]> detailsInputs = new Dictionary<string, string[]>{
            { "cidlink", new string[] {
                "cidlink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "gpp", new string[] {
                "gpp",
                "https://luausoft.com",
                "http://localhost/",

            } },
            { "blink", new string[] {
                "blink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "ylink", new string[] {
                "ylink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "alink", new string[] {
                "alink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "flink", new string[] {
                "flink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "tlink", new string[] {
                "tlink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "ilink", new string[] {
                "ilink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "vkontakte", new string[] {
                "vkontakte",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "tylink", new string[] {
                "tylink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "lilink", new string[] {
                "lilink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "plink", new string[] {
                "plink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "dblink", new string[] {
                "dblink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "wlink", new string[] {
                "wlink",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "wdata", new string[] {
                "wdata",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "gmbdash", new string[] {
                "gmbdash",
                "https://business.google.com/edit/l/03939859455138570231/",
                "https://business.google.com/edit/l/03939859455138570231",
            } },
            { "gplus_post_link", new string[] {
                "gplus_post_link",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "scrapbook_photos_link", new string[] {
                "scrapbook_photos_link",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "ytlogin", new string[] {
                "ytlogin",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "ytpass", new string[] {
                "ytpass",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "ytrd", new string[] {
                "ytrd",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "thirdpartyacc", new string[] {
                "thirdpartyacc",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "thirdpartypass", new string[] {
                "thirdpartypass",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "binglogin", new string[] {
                "binglogin",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "bingpass", new string[] {
                "bingpass",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "bingdash", new string[] {
                "bingdash",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "applemapslogin", new string[] {
                "applemapslogin",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "applemapspass", new string[] {
                "applemapspass",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "mapsconnect", new string[] {
                "mapsconnect",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "yandexlogin", new string[] {
                "yandexlogin",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "yandexpass", new string[] {
                "yandexpass",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "yandexdash", new string[] {
                "yandexdash",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "business_name", new string[] {
                "business_name",
                "Test Business: Luau Group",
                "Luau Group",
            } },
            { "local_language_name", new string[] {
                "local_language_name",
                "Luau Group",
                "Luau",
            } },
            { "address_1", new string[] {
                "address_1",
                "ulitsa General Kolev 113",
                "ulitsa General Kolev",
            } },
            { "address_2", new string[] {
                "address_2",
                "9",
                "18",
            } },
            { "country", new string[] {
                "country",
                "Bulgaria",
                "bg",
            } },
            { "city", new string[] {
                "city",
                "Varna",
                "Vn",
            } },
            { "district", new string[] {
                "district",
                "Levski",
                "lewski",
            } },
            { "state", new string[] {
                "state",
                "Varna",
                "Vn",
            } },
            { "zip", new string[] {
                "zip",
                "9002",
                "9000",
            } },
            { "destination", new string[] {
                "destination",
                "Varna",
                "vn",
            } },
            { "english_address", new string[] {
                "english_address",
                "ulitsa General Kolev 113",
                "ulitsa General Kolev",
            } },
            { "phone", new string[] {
                "phone",
                "+39-06-42290012",
                "+39-06-422900",
            } },
            { "fax", new string[] {
                "fax",
                "@luaugroup.com",
                "@luaugroup.c",
            } },
            { "re", new string[] {
                "re",
                "rand@gmail.com",
                "rand2@gmail.com",
            } },
            { "adobetracking", new string[] {
                "adobetracking",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "brandcom", new string[] {
                "brandcom",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "website", new string[] {
                "website",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "lps", new string[] {
                "lps",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "llb", new string[] {
                "llb",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "newssite", new string[] {
                "newssite",
                "https://luausoft.com",
                "http://localhost/",
            } },
            { "rooms", new string[] {
                "rooms",
                "0",
                "50",
            } },
            { "pcoords", new string[] {
                "pcoords",
                "43.218913, 27.923657",
                "43.217717, 27.924086",
            } },
            { "yearofest", new string[] {
                "yearofest",
                "1922",
                "2000",
            } },
            { "starrate", new string[] {
                "starrate",
                "0",
                "5",
            } },
            { "taglines", new string[] {
                "taglines",
                "randomtext",
                "randomtext2",
            } },
            { "rebconv", new string[] {
                "rebconv",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "usp", new string[] {
                "usp",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "bdesc", new string[] {
                "bdesc",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "rr", new string[] {
                "rr",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "ongoing_issues", new string[] {
                "ongoing_issues",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "creq", new string[] {
                "creq",
                "asdasdasd",
                "fsdfdsf",
            } },
            { "succ", new string[] {
                "succ",
                "asdasdasd",
                "fsdfdsf",
            } }
        };

        public static void GivingViewAccess(IWebDriver instance, string hotelName = "luau")
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys(hotelName);

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            Thread.Sleep(2500);

            LogAction("Click on the tags selectize");
            LocateElement(instance, By.Id("userAccess-selectized"), 10).SendKeys(Keys.Down);

            LogAction("Clear the selectize");
            LocateElement(instance, By.Id("userAccess-selectized"), 10).Clear();

            LogAction("Start writing nap");
            LocateElement(instance, By.Id("userAccess-selectized"), 10).SendKeys("operator");

            LogAction("Click on the suggested tag");
            LocateElement(instance, By.Id("userAccess-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Close the selectize");
            LocateElement(instance, By.Id("userAccess-selectized"), 10).SendKeys(Keys.Escape);

            Thread.Sleep(1500);

            LogAction("Click on the give access button");
            LocateElement(instance, By.XPath("//*[@id='businessAccessAdd']"), 10).Click();

        }

        public static void CheckViewAccess(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            Thread.Sleep(2500);

            LogAction("Check if administration can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='listing_administration_save']/a"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }

            LogAction("Click on the details section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='details']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if details can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }

            /*LogAction("Click on the rankings section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='keywords']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if rankings can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='keyword_add_wrapper']/a"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }*/

            LogAction("Click on the citations section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='directories']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if citations can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='directory_add_wrapper']/a"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }

            LogAction("Click on the venues section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='venues']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if venues can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[2]/button"), 10);
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }

            LogAction("Click on the comments section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='comments']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if comments can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[1]/button"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }
        }

        public static void GivingEditAccess(IWebDriver instance, string hotelName = "luau")
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys(hotelName);

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            Thread.Sleep(2500);

            try
            {
                LogAction("Click to give business access");
                LocateElement(instance, By.XPath("//*[@id='businessAccess']/div[2]/div[2]/div/div[2]/div"), 10).Click();
            }
            catch
            {
                Console.WriteLine("Failed at attempt3");
            }
        }

        public static void RemoveAccess(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            Thread.Sleep(2500);

            try
            {
                LogAction("Click to give business access");
                LocateElement(instance, By.XPath("//*[@id='businessAccess']/div[2]/div[2]/div/div[2]/div"), 10).Click();
            }
            catch
            {
                Console.WriteLine("Failed at attempt3");
            }

        }

        public static void CheckEditAccess(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the details section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='details']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(1000);

            LogAction("Check if details can be saved");
            LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();

            Thread.Sleep(3000);

            /*LogAction("Click on the rankings section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='keywords']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if rankings can be saved");
            try
            {
                LocateElement(instance, By.XPath("//*[@id='keyword_add_wrapper']/a"), 10).Click();
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("Cant save");
            }*/

            LogAction("Click on the citations section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='directories']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(1000);

            LogAction("Check if citations can be saved");
            LocateElement(instance, By.XPath("//*[@id='directory_add_wrapper']/a"), 10).Click();

            Thread.Sleep(8000);

            LogAction("Close the modal for directory adding");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[10]/button[1]"), 10).Click();

            Thread.Sleep(3000);

            LogAction("Click on the venues section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='venues']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(1000);

            LogAction("Check if venues can be saved");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[2]/button"), 10).Click();

            Thread.Sleep(3000);

            LogAction("Click on the comments section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='comments']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if comments can be saved");
            LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[1]/button"), 10).Click();

            Thread.Sleep(3000);
        }

        public static void VenuesTest(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Filter by hotel name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the first result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            try
            {
                LogAction("Click on the venues tab");
                LocateElement(instance, By.XPath("//*[@id='venues']/a"), 10).Click();
            }
            catch
            {

                if(instance.Url == ConfigurationManager.AppSettings["url"] + "/business")
                {
                    IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement);

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Wait for loader to disappear");
                    WaitForLoader(instance);

                    LogAction("Click on the venues tab");
                    LocateElement(instance, By.XPath("//*[@id='venues']/a"), 10).Click();
                }
                else
                {
                    Thread.Sleep(3500);

                    IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='venues']/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement);  
                }
                
            }
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Input name to the new venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li/div[1]/div[1]/div/input"), 10).SendKeys("RandomName");

            LogAction("Save venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[2]/button"), 10).Click();

            string baseURL = ConfigurationManager.AppSettings["url"];

            LogAction("Input link to the existing venue");
            LocateElement(instance, By.XPath("//*[@id='contact_email_']"), 10).SendKeys(baseURL + "business/5634");

            LogAction("Input note to the existing venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li[1]/div[2]/div[1]/div/textarea"), 10).SendKeys("RandomNote");

            LogAction("Input work around the web to the existing venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li[1]/div[2]/div[2]/div/textarea"), 10).SendKeys("Randomtextsatas");

            LogAction("Click on the status selectize");
            LocateElement(instance, By.XPath("//*[@id='statuses_div']/div[2]/div"), 10).Click();

            LogAction("Click on the managed option and select it");
            LocateElement(instance, By.XPath("//*[@id='statuses_div']/div[2]/div/div[2]/div/div[3]"), 10).Click();

            LogAction("Save venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li[1]/div[3]/button[1]"), 5).Click();

            LogAction("Clear name field for the venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li/div[1]/div[1]/div/input"), 10).SendKeys("");

            LogAction("Save venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li[1]/div[3]/button[1]"), 5).Click();

            LogAction("Clear link for the venue");
            LocateElement(instance, By.XPath("//*[@id='contact_email_']"), 10).SendKeys("");

            LogAction("Save venue");
            LocateElement(instance, By.XPath("//*[@id='venues_tab']/div/div[1]/ul/li[1]/div[3]/button[1]"), 5).Click();

            LogAction("Click on the delete button");
            LocateElement(instance, By.XPath("//button[contains(.,'Delete')]"), 5).Click();

            LogAction("Click ok to confirm the deletion");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 5).Click();
            //*[@id="1949"]
        }

        public static void CommentsTest(IWebDriver instance)
        {
            LogAction("Navigate to business page");
            LocateElement(instance, By.XPath("//*[@id='businesses_tab']/a"), 10);

            LogAction("Click on the first business");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            Thread.Sleep(1500);

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance);

            LogAction("Click Comments Tab");
            LocateElement(instance, By.XPath("//*[@id='comments']/a"), 7).Click();

            LogAction("Click on the publish comment button");
            LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Please select a Tag before publishing your comment. Thank you.'])[1]/following::button[1]"), 10).Click();

            LogAction("Click on the tags selectize");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).SendKeys(Keys.Down);

            LogAction("Clear the selectize");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).Clear();

            LogAction("Start writing nap");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).SendKeys("nap");

            LogAction("Click on the suggested tag");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Clear the writing field");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).Clear();

            LogAction("Start writing dup");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).SendKeys("dup");

            LogAction("Click on the suggested tag");
            LocateElement(instance, By.Id("listing_comment_tags-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Create random comment text");
            string newComment = Path.GetRandomFileName();

            LogAction("Input the text into the new comment field");
            LocateElement(instance, By.Id("newComment"), 10).SendKeys(newComment);

            LogAction("Submit the comment");
            LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[1]/button"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Copy the new comment's username for further check");
            string username = LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='edit'])[4]/following::h4[1]"), 10).Text;

            LogAction("Click the edit button on the last comment");
            LocateElement(instance, By.XPath("//*[@id='commentsWrapper']/div/ul/li[5]/article/header/a"), 10).Click();

            LogAction("Generate a new comment text");
            string editComment = Path.GetRandomFileName();

            LogAction("Input the new comment");
            LocateElement(instance, By.Id("editComment"), 10).Clear();
            LocateElement(instance, By.Id("editComment"), 10).SendKeys(editComment);

            LogAction("Click on the tags selectize");
            LocateElement(instance, By.Id("edit_comment_tags-selectized"), 10).SendKeys(Keys.Down);

            LogAction("Click on the tags selectize");
            LocateElement(instance, By.Id("edit_comment_tags-selectized"), 10).Clear();
            LogAction("Start writing canc");
            LocateElement(instance, By.Id("edit_comment_tags-selectized"), 10).SendKeys("canc");
            LogAction("Click on the suggested tag");
            LocateElement(instance, By.Id("edit_comment_tags-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the tags selectize");
            LocateElement(instance, By.XPath("//*[@id='edit_comment']/div/div[4]/div/div"), 10).Click();
            LogAction("Delete one tag from the selectize");
            LocateElement(instance, By.XPath("//*[@id='edit_comment']/div/div[4]/div/div/div/div[2]/a"), 10).Click();
            LogAction("close the selectize window");
            LocateElement(instance, By.XPath("//*[@id='edit_comment']/div/div[1]"), 10).Click();

            LogAction("Submit the changes to the comment");
            LocateElement(instance, By.XPath("//*[@id='edit_comment']/div/ul/li/button"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Click on the view all button");
            LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[3]/button"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Check if the comment you just wrote is with the same text as the last one in the view all modal");
            Assert.AreEqual(editComment, LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='edit'])[6]/following::p[2]"), 10).Text);

            LogAction("Filter by username");
            LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[3]/following::input[1]"), 10).SendKeys(username);

            LogAction("Check if there are comments with the username of the comment you just wrote");
            Assert.AreEqual(username, LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[3]/following::h4[1]"), 10).Text);

            LogAction("Close the view all modal");
            LocateElement(instance, By.XPath("//*[@id='allCommentsContent']/button"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Click on the prev button to see old comments");
            LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[2]/button"), 10).Click();
            Thread.Sleep(2500);

            LogAction("Check if the last written comment doesnt matches the last one on the page");
            Assert.AreNotEqual(editComment, LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='edit'])[5]/following::p[2]"), 10).Text);

            LogAction("Click on the next button to go back to the previous page");
            LocateElement(instance, By.XPath("//*[@id='comments_tab']/div/div[3]/ul/li[2]/button"), 10).Click();
            Thread.Sleep(2000);

            LogAction("Check if the last written comment matches the last one on the page");
            Assert.AreEqual(editComment, LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='edit'])[5]/following::p[2]"), 10).Text);
        }

        public static void DetailsTest(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Filter by hotel name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the first result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            try
            {
                LogAction("Click on the details tab");
                LocateElement(instance, By.XPath("//*[@id='details']/a"), 10).Click();
            }
            catch
            {
                if (instance.Url == ConfigurationManager.AppSettings["url"] + "/business")
                {
                    IWebElement webElement3 = LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement3);

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Wait for loader to disappear");
                    WaitForLoader(instance);

                    LogAction("Click on the details tab");
                    LocateElement(instance, By.XPath("//*[@id='details']/a"), 10).Click();
                }
                else
                {
                    Thread.Sleep(3500);

                    LogAction("Click on the details tab with js");
                    IWebElement webElement4 = LocateElement(instance, By.XPath("//*[@id='details']/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement4);
                }
            }

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Verify that we are in the right listing page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='listingViewTopToolbar']/div/div/h3/span"), 10).Text, Does.Contain("Luau"));

            LogAction("Verify that the details tab is loaded");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='details_form']/h4[1]"), 10).Text, Does.Contain("Business Links"));
            /*
            foreach (var input in detailsInputs)
            {
                LocateElement(instance, By.Name(input.Value[0]), 10).Clear();
                LocateElement(instance, By.Name(input.Value[0]), 10).SendKeys(input.Value[2]);
            }
            
            LocateElement(instance, By.XPath("//*[@id='amenity_comments']"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='website_notes']"), 10).SendKeys("random text");

            LocateElement(instance, By.XPath("//*[@id='website_notes']"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='website_notes']"), 10).SendKeys("random text");

            Thread.Sleep(1500);

            LogAction("Click on the save button");
            LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            foreach (var input in detailsInputs)
            {
                LocateElement(instance, By.Name(input.Value[0]), 10).Clear();
                LocateElement(instance, By.Name(input.Value[0]), 10).SendKeys(input.Value[1]);
            }

            LocateElement(instance, By.XPath("//*[@id='website_notes']"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='website_notes']"), 10).SendKeys("random text22222222");

            Thread.Sleep(1500);*/

            LogAction("Click on the save button");
            LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Details data saved successfully"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            Thread.Sleep(2500);

            LogAction("Upload the file");
            string filePath = @"C:\123.txt";
            instance.FindElement(By.Id("file_upload-select")).SendKeys(filePath);

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 25).Text, Does.Contain("Upload Completed"));
            
            LogAction("Close the green notication for successfull file upload");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 25).Click();

            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='attachmentIconWrapper']/div[1]/i"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click ok to confirm the deletion");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 5).Click();

            try
            {
                LogAction("Clear gmb pass");
                LocateElement(instance, By.Name("gmbpass"), 10).Clear();

                Assert.IsTrue(false);
            }
            catch
            {
                LogAction("GMB Pass cant be edited like it has to");
            }

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Write something on the selectize for business types");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys("airpor");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Delete the category we wrote");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys(Keys.Backspace);

            LogAction("Write something on the selectize for business types");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys("arena");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Write something on the selectize for business types");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys("golf");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("businesstypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the save button");
            LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Click to copy the business types");
            LocateElement(instance, By.XPath("//*[@id='details_form']/div[8]/div[2]/button"), 15).Click();

            LogAction("Write something on the selectize for payments");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys("american");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Delete the category we wrote");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys(Keys.Backspace);

            LogAction("Write something on the selectize for payment types");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys("cash");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Write something on the selectize for payments types");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys("duet");

            LogAction("Select the suggested type");
            LocateElement(instance, By.Id("paymenttypes-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the save button");
            LocateElement(instance, By.XPath("//*[@id='listing_details_save']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Click ok to close modal");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Click to copy the payment types");
            LocateElement(instance, By.XPath("//*[@id='details_form']/div[13]/div[2]/button"), 15).Click();

            LogAction("Clear the taglines input");
            LocateElement(instance, By.Name("taglines"), 10).Clear();

            LogAction("Click on the button to edit amenities");
            LocateElement(instance, By.XPath("//*[@id='details_form']/div[16]/button"), 10).Click();

            LogAction("Click to make the wifi paid");
            LocateElement(instance, By.XPath("//*[@id='modal_default']/div/div/div/div/div[1]/div[2]/div/span[1]/span[2]/label"), 10).Click();

            LogAction("Click to check indoor pool");
            LocateElement(instance, By.XPath("//*[@id='modal_default']/div/div/div/div/div[6]/div[2]/div/span[1]/span[1]/label"), 10).Click();

            LogAction("Click to close the modal");
            IWebElement webElement2 = LocateElement(instance, By.CssSelector("#modal_default"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement2);

            Thread.Sleep(1500);
        }

        public static void AdministrationTest(IWebDriver instance)
        {
            var userArray = new Dictionary<string, string> {
                { "fname", "John" },
                { "lname", "Doe" },
                { "email", "j.doe@gmail.com" },
                { "position", "Manager" }
            };

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Filter the table by business name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

            LogAction("Click on the filtered result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr[1]/td[1]/a/span"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            Thread.Sleep(5000);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='administration']/a"), 10).Click();

            Thread.Sleep(2500);

            try
            {
                LogAction("Filter the table by business name");
                LocateElement(instance, By.Id("listing_id"), 10).SendKeys("123124");
                Assert.IsTrue(false);
            }
            catch
            {
                Console.WriteLine("cant edit listing id");
            }

            foreach (var pair in administrationArray)
            {
                string key = pair.Key;
                string[] value = pair.Value;

                try
                {
                    LogAction("Click on the " + key + " selectize");
                    LocateElement(instance, By.XPath(value[1]), 10).Click();
                }
                catch
                {
                    LogAction("Click on the " + key + " selectize 2nd attempt");
                    LocateElement(instance, By.XPath(value[2]), 10).Click();
                }
                LogAction("Delete if there is something selected");
                LocateElement(instance, By.XPath(value[2]), 10).SendKeys(Keys.Backspace);

                LogAction("Input text");
                LocateElement(instance, By.XPath(value[2]), 10).SendKeys(value[0]);

                LogAction("Select suggested " + key);
                LocateElement(instance, By.XPath(value[2]), 10).SendKeys(Keys.Enter);

                LogAction("Click to save administration");
                LocateElement(instance, By.XPath("//*[@id='listing_administration_save']/a"), 10).Click();

                LogAction("Check if the notification message is success");
                Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div"), 10).Text, Does.Contain("saved successfully"));

                LogAction("Close the success modal");
                LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();
            }

            LogAction("Click on the add hotel contact button");
            LocateElement(instance, By.XPath("//*[@id='administration_form']/div[8]/div/button"), 10).Click();

            LogAction("wait for the modal opening");
            Thread.Sleep(1500);

            LogAction("Input first name");
            LocateElement(instance, By.XPath("//*[@id='contact_add_first_name']"), 10).SendKeys(userArray["fname"]);

            LogAction("Input last name");
            LocateElement(instance, By.XPath("//*[@id='contact_add_last_name']"), 10).SendKeys(userArray["lname"]);

            LogAction("Input email");
            LocateElement(instance, By.XPath("//*[@id='contact_add_email']"), 10).SendKeys(userArray["email"]);

            LogAction("Input position");
            LocateElement(instance, By.XPath("//*[@id='contact_add_position']"), 10).SendKeys(userArray["position"]);

            LogAction("Click the add contact button");
            LocateElement(instance, By.XPath("//*[@id='save_directory']"), 10).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div"), 10).Text, Does.Contain("added successfully"));

            LogAction("Close the success modal");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Click to convert the hotel contact to user");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='hotel_contacts_list']/div[2]/label[1]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2500);

            LogAction("Click on the edit button to go to the profile of the contact");
            LocateElement(instance, By.XPath("//*[@id='hotel_contacts_list']/div[2]/a"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click to delete the hotel contact");
            LocateElement(instance, By.XPath("//*[@id='hotel_contacts_list']/div[2]/label[2]"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Close the success modal");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 15).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Check if the name of the user matches the one we wrote");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_edit_uname']"), 10).Text, Does.Contain(userArray["fname"] + " " + userArray["lname"]));

            LogAction("Check if the position is matching");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_edit_position']"), 10).Text, Does.Contain(userArray["position"]));
        }

        public static void RankingsTest(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Filter by hotel name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("adana");

            LogAction("Click on the first result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            try
            {
                LogAction("Click on the rankings tab");
                LocateElement(instance, By.XPath("//*[@id='keywords']/a"), 10).Click();
            }
            catch
            {
                if (instance.Url == ConfigurationManager.AppSettings["url"] + "/business")
                {
                    IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Wait for loader to disappear");
                    WaitForLoader(instance, 20);

                    LogAction("Click on the rankings tab");
                    LocateElement(instance, By.XPath("//*[@id='keywords']/a"), 10).Click();
                }
                else
                {
                    Thread.Sleep(3500);

                    LogAction("Click on the rankings tab with js");
                    IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='keywords']/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement2);
                }
            }

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 20);

            Thread.Sleep(3000);

            int keywordName = new Random().Next(0, 1000);

            LogAction("Click on the filtering th for keyword name");
            LocateElement(instance, By.XPath("//*[@id='keywords-table']/thead/tr/th[1]/input"), 10).Click();

            LogAction("Click on add keyword button");
            LocateElement(instance, By.XPath("//*[@id='keyword_add_wrapper']/a"), 10).Click();

            Thread.Sleep(2000);

            LogAction("Input keyword name");
            LocateElement(instance, By.XPath("//*[@id='keyword_name']"), 10).SendKeys("" + keywordName);

            LogAction("Input keyword volume");
            LocateElement(instance, By.XPath("//*[@id='keyword_volume']"), 10).SendKeys("230");

            LogAction("Click on the type selectize");
            LocateElement(instance, By.XPath("//*[@id='keyword_type-selectized']"), 10).Click();

            LogAction("Delete if there is something selected");
            LocateElement(instance, By.XPath("//*[@id='keyword_type-selectized']"), 10).SendKeys(Keys.Backspace);

            LogAction("Input text");
            LocateElement(instance, By.XPath("//*[@id='keyword_type-selectized']"), 10).SendKeys("brand");

            LogAction("Select suggested type");
            LocateElement(instance, By.XPath("//*[@id='keyword_type-selectized']"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the tld selectize");
            LocateElement(instance, By.XPath("//*[@id='keyword_tld-selectized']"), 10).Click();

            LogAction("Delete if there is something selected");
            LocateElement(instance, By.XPath("//*[@id='keyword_tld-selectized']"), 10).SendKeys(Keys.Backspace);

            LogAction("Input text");
            LocateElement(instance, By.XPath("//*[@id='keyword_tld-selectized']"), 10).SendKeys(".tr");

            LogAction("Select suggested tld");
            LocateElement(instance, By.XPath("//*[@id='keyword_tld-selectized']"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the save keyword button");
            LocateElement(instance, By.XPath("//*[@id='keyword_edit_modal']/div/div[6]/button[2]"), 10).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Success"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Filter table by keyword name");
            LocateElement(instance, By.XPath("//*[@id='keywords-table']/thead/tr/th[1]/input"), 10).SendKeys("" + keywordName);

            LogAction("Click to edit the first result");
            LocateElement(instance, By.XPath("//*[@id='keywords-table']/tbody/tr[1]/td[8]/i[1]"), 15).Click();

            Thread.Sleep(1500);

            LogAction("Input keyword name");
            LocateElement(instance, By.XPath("//*[@id='keyword_name']"), 10).SendKeys("test");

            LogAction("Click on the save keyword button");
            LocateElement(instance, By.XPath("//*[@id='keyword_edit_modal']/div/div[6]/button[2]"), 10).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Success"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Click on the delete button for the first result");
            LocateElement(instance, By.XPath("//*[@id='keywords-table']/tbody/tr[1]/td[8]/i[3]"), 15).Click();

            LogAction("Click ok to confirm deletion");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 15).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Success"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();
        }

        public static void CitationsTest(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Filter by hotel name");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/thead/tr/th[1]/input"), 10).SendKeys("adana");

            LogAction("Click on the first result");
            LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            try
            {
                LogAction("Click on the citations tab");
                LocateElement(instance, By.XPath("//*[@id='directories']/a"), 10).Click();
            }
            catch
            {
                if (instance.Url == ConfigurationManager.AppSettings["url"] + "/business")
                {
                    LogAction("Click on the first result 2nd attempt");
                    IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='businesses-table']/tbody/tr/td[1]/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Wait for loader to disappear");
                    WaitForLoader(instance);

                    LogAction("Click on the citations tab");
                    LocateElement(instance, By.XPath("//*[@id='directories']/a"), 10).Click();
                }
                else
                {
                    Thread.Sleep(3500);

                    LogAction("Click on the citations tab with js");
                    IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='directories']/a"), 10);
                    instance.ExecuteJavaScript("arguments[0].click();", webElement2);
                }
            }

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Click on the add citation button");
            LocateElement(instance, By.XPath("//*[@id='directory_add_wrapper']/a"), 10).Click();

            LogAction("Click on the name selectize");
            LocateElement(instance, By.XPath("//*[@id='directory_nameID-selectized']"), 10).Click();

            LogAction("Delete if there is something selected");
            LocateElement(instance, By.XPath("//*[@id='directory_nameID-selectized']"), 10).SendKeys(Keys.Backspace);

            LogAction("Input text");
            LocateElement(instance, By.XPath("//*[@id='directory_nameID-selectized']"), 10).SendKeys("cityseeker.com");

            LogAction("Select suggested tld");
            LocateElement(instance, By.XPath("//*[@id='directory_nameID-selectized']"), 10).SendKeys(Keys.Enter);

            LogAction("Input citation link");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[4]/div/input"), 10).SendKeys("sadasd");

            LogAction("Input citation note");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[7]/div/input"), 10).SendKeys("ewrtyhetj");

            LogAction("Click on the status selectize");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[8]/div/div/div[1]/input"), 10).Click();

            LogAction("Delete if there is something selected");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[8]/div/div/div[1]/input"), 10).SendKeys(Keys.Backspace);

            LogAction("Input text");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[8]/div/div/div[1]/input"), 10).SendKeys("submitted");

            LogAction("Select suggested tld");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[8]/div/div/div[1]/input"), 10).SendKeys(Keys.Enter);

            LogAction("Click on the save button");
            LocateElement(instance, By.XPath("//*[@id='directory_edit_modal']/div/div[10]/button[2]"), 10).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Success"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();

            LogAction("Click to filter the directories by name");
            LocateElement(instance, By.XPath("//*[@id='directories-table']/thead/tr/th[1]/input"), 10).SendKeys("seek");

            LogAction("Click to delete the first result");
            LocateElement(instance, By.XPath("//*[@id='directories-table']/tbody/tr[1]/td[7]/i[2]"), 10).Click();

            Thread.Sleep(2000);

            LogAction("Click to confirm deletion");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 15).Click();

            LogAction("Check if the notification message is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Success"));

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 15).Click();
        }
    }
}



