﻿using System;
using System.Configuration;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class ClientPages : HelpFunctions
    {
        public static void ReportingGMB(IWebDriver instance, bool singleHotel = false)
        {
            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            switch (singleHotel)
            {
                case true:
                    LogAction("Make sure we are on the luau listing page");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='basic-info']/tbody/tr/td[1]/span"), 10).Text, Does.Contain("Luau"));

                    LogAction("Click on the reporting menu in the sidebar");
                    try
                    {
                        LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();
                    }
                    catch
                    {
                        IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);
                    }

                    LogAction("Click on the gmb reporting option in the sidebar");
                    try
                    {
                        LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10).Click();
                    }
                    catch
                    {
                        IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);
                    }
                    break;
                default:
                    LogAction("Set the hotelCount to variable");
                    int hotelsCount = Int32.Parse(ConfigurationManager.AppSettings["multiHotelCount"]);

                    LogAction("Check that the number of hotels in the table is right");
                    string tableInfo = LocateElement(instance, By.XPath("//*[@id='active-table_info']"), 10).Text;
                    Assert.IsFalse(tableInfo.Contains("Showing 0 to 0 of 0 entries"),"Table must not be empty");
                    Assert.IsFalse(tableInfo.Contains("Showing 1 to 1 of 1 entries"), "Table must have more than 1 row");

                    LogAction("Click on the reporting menu in the sidebar");
                    LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();

                    try
                    {
                        LogAction("Click on the gmb reporting option in the sidebar");
                        LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10).Click();

                        LogAction("wait for the loader to disappear");
                        WaitForLoader(instance);

                        LogAction("Check if we are on the multi insights page");
                        Assert.That(LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Clients Documents'])[1]/following::h3[1]"), 20).Text, Does.Contain("Google My Business Insights"));
                    }
                    catch
                    {
                        LogAction("Click on the reporting menu in the sidebar with JS");
                        IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                        LogAction("Click on the gmb reporting option in the sidebar with JS");
                        webElement2 = LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                        LogAction("Check if we are on the multi insights page 2nd attempt");
                        Assert.That(LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Clients Documents'])[1]/following::h3[1]"), 20).Text, Does.Contain("Google My Business Insights"));
                    }

                    try
                    {
                        LogAction("Click on the notificiation to close it");
                        LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Click();
                    }
                    catch
                    {
                    }
                    
                    Thread.Sleep(1500);

                    string tableXPath = "//*[@id='reporting-gmb']";

                    TableColumns.ReportingGMB(instance);

                    LeftNavigation.TestTableVisibility(instance, tableXPath);

                    LogAction("Check that the number of hotels in the table is right");
                    tableInfo = LocateElement(instance, By.XPath("//*[@id='reporting-gmb']"), 10).Text;
                    Assert.IsFalse(tableInfo.Contains("Showing 0 to 0 of 0 entries"), "Table must not be empty");
                    Assert.IsFalse(tableInfo.Contains("Showing 1 to 1 of 1 entries"), "Table must have more than 1 row");

                    try
                    {
                        LogAction("click to close the warning notification");
                        LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
                    }
                    catch
                    {

                    }

                    LogAction("Click on the hotel name to open infocard");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/tbody/tr[1]/td[1]/a"), 10).Click();
                    Thread.Sleep(1500);

                    LogAction("Check if the hotel name in the infocard matches the name we clicked");
                    String firstHotel = LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/tbody/tr[1]/td[1]/a"), 10).Text;
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='infocard_companyname']"), 10).Text, Does.Contain(firstHotel));

                    var modal = instance.FindElement(By.XPath("//*[@id='infocard_modal']/div"));

                   /* int videoSize = modal.Size.Height;

                    if (videoSize > 700)
                    {
                        Assert.IsTrue(false);
                    }
                    Console.WriteLine(videoSize);*/

                    LogAction("Close the infocard");
                    LocateElement(instance, By.XPath("//*[@id='infocard_modal']/div/div[2]/button"), 10).Click();
                    Thread.Sleep(1500);

                    LogAction("Click on export to excel button");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/button[2]"), 10).Click();

                    LogAction("Clear Search All Data field");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb_filter']/div/input"), 10).Clear();

                    LogAction("Search Loze in Search All Data field");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb_filter']/div/input"), 10).SendKeys("Нептун");

                    LogAction("Check if in the results there is something");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/tbody/tr/td[1]/a"), 20).Text, Does.Contain("Нептун"));

                    LogAction("Clear Search All Data field");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb_filter']/div/input"), 10).Clear();

                    LogAction("Search au in Search All Data field");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb_filter']/div/input"), 10).SendKeys("Luau");

                    LogAction("Check if in the results there is something");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/tbody/tr/td[1]/a"), 20).Text, Does.Contain("Luau"));

                    LogAction("Go to Luau's gmb page");
                    LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/tbody/tr/td[23]/a/button"), 10).Click();

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Maximize browser window");
                    instance.Manage().Window.Maximize();
                    break;
            }
            
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);
            
            LogAction("Check if we are on the single insights page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='main-md-card']/div[2]/div[1]/div/div[1]/h3"), 20).Text, Does.Contain("GMB Insights"));

            LogAction("Check if we are on the page for Luau");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='main-md-card']/div[2]/div[1]/div/div[1]/span/a"), 20).Text, Does.Contain("Luau"));

            try
            {
                LogAction("click to close the warning notification");
                LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
            }
            catch
            {
            }

            //Customer Searches - Direct or Discovery
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date1']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date1']"), 10).SendKeys("2017-10");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date1']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date1']"), 10).SendKeys("2018-03");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[1]/div/div[2]/div/div[4]/button"), 10).Click();

            //Views Search and Maps
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date2']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date2']"), 10).SendKeys("2017-04");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date2']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date2']"), 10).SendKeys("2017-10");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[3]/div/div[2]/div/div[4]/button"), 10).Click();

            //Customer actions
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date3']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date3']"), 10).SendKeys("2017-06");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date3']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date3']"), 10).SendKeys("2018-01");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[5]/div/div[2]/div/div[4]/button"), 10).Click();

            //Phone Calls
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date4']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date4']"), 10).SendKeys("2018-01");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date4']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date4']"), 10).SendKeys("2018-04");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[7]/div/div[2]/div/div[4]/button"), 10).Click();

            //Photo Views
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date5']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date5']"), 10).SendKeys("2017-10");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date5']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date5']"), 10).SendKeys("2018-05");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[9]/div/div[2]/div/div[4]/button"), 10).Click();

            //Photo Quantity
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date6']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date6']"), 10).SendKeys("2018-01");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date6']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date6']"), 10).SendKeys("2018-02");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[11]/div/div[2]/div/div[4]/button"), 10).Click();

            //Customer Searches - Direct or Discovery
            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date1']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='start_date1']"), 10).SendKeys("2017-08");

            LogAction("Clear the preselected text in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date1']"), 10).Clear();

            LogAction("Input date in the datepicker");
            LocateElement(instance, By.XPath("//*[@id='end_date1']"), 10).SendKeys("2018-02");

            LogAction("Click apply button");
            LocateElement(instance, By.XPath("//*[@id='switcher-content-a-fade']/li[1]/div[1]/div/div[2]/div/div[4]/button"), 10).Click();
        }

        public static void ReportingRankings(IWebDriver instance, bool singleHotel = false)
        {
            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Click on the reporting menu in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();

            try
            {
                LogAction("Click on the rankings reporting option in the sidebar");
                LocateElement(instance, By.XPath("//*[@id='rankings_reporting_tab']/a"), 10).Click();

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance);

                if (singleHotel == false)
                {
                    LogAction("Check if we are on the rankings page");
                    LocateElement(instance, By.XPath("//*[@id='rankings_reporting_tab']/a"), 20);
                }
                else
                {
                    LogAction("Check if we are on the rankigns page v2");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div[3]/div[1]/div/div/span/a"), 10);
                }

            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the rankings reporting option in the sidebar with JS");
                webElement2 = LocateElement(instance, By.XPath("//*[@id='rankings_reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                if (singleHotel == false)
                {
                    LogAction("Check if we are on the rankings page");
                    LocateElement(instance, By.XPath("//*[@id='rankings_reporting_tab']/a"), 20);
                }
                else
                {
                    LogAction("Check if we are on the rankigns page v2");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div[3]/div[1]/div/div/span/a"), 10);
                }
            }

            switch (singleHotel)
            {
                case true:
                    break;
                default:
                    string tableXPath = "//*[@id='reporting-rankings']";

                    LogAction("Click on the excelling tab");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div/div[3]/div[2]/div/ul/li[2]/a"), 10).Click();

                    LogAction("Click on the underachieving tab");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div/div[3]/div[2]/div/ul/li[3]/a"), 10).Click();

                    LogAction("Click on the select in the right corner");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div/div[1]/div/div[3]/div/div[2]/div/div"), 10).Click();

                    LogAction("Mark the 3rd option");
                    LocateElement(instance, By.XPath("//*[@id='vue-app-container']/div/div[1]/div/div[3]/div/div[2]/div/div/div[2]/div/div[3]"), 10).Click();

                    LogAction("Close notification");
                    LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();

                    Thread.Sleep(3500);

                    TableColumns.ReportingRankings(instance);

                    LeftNavigation.TestTableVisibility(instance, tableXPath);

                    Thread.Sleep(3500);

                    LogAction("Filter table by the name Luau");
                    LocateElement(instance, By.XPath("//*[@id='reporting-rankings_filter']/div/input"), 10).SendKeys("Luau");

                    LogAction("Click on the first result");
                    LocateElement(instance, By.XPath("//*[@id='reporting-rankings']/tbody/tr/td[23]/a/button"), 10).Click();

                    break;
            }

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);
        }

        public static void ReportingReviews(IWebDriver instance, bool singleHotel = false)
        {
            if (singleHotel == true)
            {
                Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[1]/div/div/div[2]/div/div/h3"), 20).Text, Does.Contain("Google Reviews Management and Statistics"));
            }
            LogAction("wait for the loader to disappear");
            WaitForLoader(instance);

            LogAction("Click on the reporting menu in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();

            LogAction("Click on the review reporting option in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='reviews_reporting_tab']/a"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 60);

            try
            {
                LogAction("Check if we are on the review reporting page");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div/div[1]/h3"), 20).Text, Does.Contain("Review Reporting - Business List"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar");
                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the review reporting option in the sidebar");
                webElement = LocateElement(instance, By.XPath("//*[@id='reviews_reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Check if we are on the review reporting page");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div/div[1]/h3"), 20).Text, Does.Contain("Review Reporting - Business List"));
            }

            LogAction("Check that the number of hotels in the table is right");
            string tableInfo = LocateElement(instance, By.XPath("//*[@id='reporting-table_info']"), 10).Text;
            Assert.IsFalse(tableInfo.Contains("Showing 0 to 0 of 0 entries"), "Table must not be empty");

            if (singleHotel == false)
            {
                Assert.IsFalse(tableInfo.Contains("Showing 1 to 1 of 1 entries"), "Table must have more than 1 row");
            }

            try
            {
                LogAction("click to close the warning notification");
                LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
            }
            catch
            {
            }

            LogAction("Click on the hotel name to open infocard");
            LocateElement(instance, By.XPath("//*[@id='reporting-table']/tbody/tr[1]/td[1]/a"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Check if the hotel name in the infocard matches the name we clicked");
            String firstHotel = LocateElement(instance, By.XPath("//*[@id='reporting-table']/tbody/tr[1]/td[1]/a"), 10).Text;
            Assert.That(LocateElement(instance, By.XPath("//*[@id='infocard_companyname']"), 10).Text, Does.Contain(firstHotel));

            LogAction("Close the infocard");
            LocateElement(instance, By.XPath("//*[@id='infocard_modal']/div/div[2]/button"), 10).Click();
            Thread.Sleep(1500);

            LogAction("Search luau in Search All Data field");
            LocateElement(instance, By.XPath("//*[@id='reporting-table']/thead/tr/th[1]/div/input"), 10).SendKeys("Luau");

            LogAction("Check if in the results there is something");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='reporting-table']/tbody/tr[1]/td[1]/a"), 20).Text, Does.Contain("Luau"));

            LogAction("Go to Luau's gmb page");
            LocateElement(instance, By.XPath("//*[@id='reporting-table']/tbody/tr[1]/td[11]/a/button"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Maximize browser window");
            instance.Manage().Window.Maximize();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Check if we are on the single review management page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[1]/div/div/div[2]/div/div[1]/h3"), 20).Text, Does.Contain("Google Reviews Management and Statistics"));

            LogAction("Check if we are on the page for Luau");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='basic-info']/tbody/tr/td[1]/span"), 20).Text, Does.Contain("Luau"));
        }

        public static void ReportingRevenue(IWebDriver instance, bool singleHotel = false)
        {
            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Click on the reporting menu in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();

            try
            {
                LogAction("Click on the revenue reporting option in the sidebar");
                LocateElement(instance, By.XPath("//*[@id='revenue_reporting_tab']/a"), 10).Click();

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 120);

                LogAction("Check if we are on the revenue page");
                LocateElement(instance, By.XPath("//*[@id='revenue_reporting_tab']/a"), 10);
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("Click on the revenue reporting option in the sidebar with JS");
                webElement2 = LocateElement(instance, By.XPath("//*[@id='revenue_reporting_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement2);

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 120);

                LogAction("Check if we are on the revenue page 2nd attempt");
                LocateElement(instance, By.XPath("//*[@id='revenue_reporting_tab']/a"), 10);
            }

            switch (singleHotel)
            {
                case true:

                    break;
                default:
                    string tableXPath = "//*[@id='reporting-revenue']";

                    TableColumns.ReportingRevenue(instance);

                    LeftNavigation.TestTableVisibility(instance, tableXPath);

                    LogAction("Click on the first hotel in the table");
                    LocateElement(instance, By.XPath("//*[@id='reporting-revenue']/tbody/tr[1]/td[19]/a/button"), 10).Click();

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());
                    break;
            }

            LogAction("Wait for the loader to disappear");
            WaitForLoader(instance);

            LogAction("Click on the select for daterange");
            LocateElement(instance, By.XPath("//*[@id='businessViewContentHolder']/div/div[1]/div[1]/div[2]/div"), 10).Click();

            LogAction("Click on the half year option");
            LocateElement(instance, By.XPath("//*[@id='businessViewContentHolder']/div/div[1]/div[1]/div[2]/div/div[2]/div/div[4]"), 10).Click();

            LogAction("Click on the apply button");
            LocateElement(instance, By.XPath("//*[@id='businessViewContentHolder']/div/div[1]/div[2]/div[5]/button"), 10).Click();

            LogAction("Filter to be able to view luau listing");
            LocateElement(instance, By.Id("report-header-selectize-selectized"), 10).SendKeys("luau");

            LogAction("Select the suggested hotel");
            LocateElement(instance, By.Id("report-header-selectize-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Go to the selected hotel's review page");
            LocateElement(instance, By.XPath("//*[@id='report-header-submit']"), 10).Click();
        }

        public static void HotelReview(IWebDriver instance, string userRole, bool singleHotel = false)
        {
            switch (singleHotel)
            {
                case true:
                    break;
                default:
                    LogAction("Wait for the loader to disappear");
                    WaitForLoader(instance, 30);

                    LogAction("Make sure we are on the review management page");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div/div/div/div[1]/h3"), 10).Text, Does.Contain("Review Management"));

                    LogAction("Check that the number of hotels in the table is right");
                    string tableInfo = LocateElement(instance, By.XPath("//*[@id='active-table_info']"), 10).Text;
                    Assert.IsFalse(tableInfo.Contains("Showing 0 to 0 of 0 entries"), "Table must not be empty");
                    Assert.IsFalse(tableInfo.Contains("Showing 1 to 1 of 1 entries"), "Table must have more than 1 row");

                    try
                    {
                        LogAction("click to close the warning notification");
                        LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
                    }
                    catch
                    {
                    }

                    LogAction("Click on the hotel name to open infocard");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr[1]/td[1]/a"), 10).Click();
                    Thread.Sleep(1500);

                    LogAction("Check if the hotel name in the infocard matches the name we clicked");
                    String firstHotel = LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr[1]/td[1]/a"), 10).Text;
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr[1]/td[1]/a"), 10).Text, Does.Contain(firstHotel));

                    LogAction("Close the infocard");
                    LocateElement(instance, By.XPath("//*[@id='infocard_modal']/div/div[2]/button"), 10).Click();
                    Thread.Sleep(1500);

                    LogAction("Click on export to excel");
                    LocateElement(instance, By.XPath("//*[@id='active-table_wrapper']/div[1]/div[1]/div/button[2]"), 10).Click();

                    LogAction("Click on the business name filter");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/thead/tr/th[1]/input"), 10).Click();

                    LogAction("Clear the filter");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/thead/tr/th[1]/input"), 10).Clear();

                    LogAction("Input text in the filter");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/thead/tr/th[1]/input"), 10).SendKeys("luau");

                    LogAction("Check if filtered result contains the word Luau");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr[1]/td[1]/a"), 10).Text, Does.Contain("Luau"));

                    LogAction("Click on the first hotel");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr/td[8]/div/a/button"), 10).Click();

                    LogAction("Input text in the filter");
                    LocateElement(instance, By.XPath("//*[@id='active-table']/thead/tr/th[1]/input"), 10).Clear();

                    string tableXPath = "//*[@id='active-table']";

                    TableColumns.ReviewManagementMulti(instance);

                    LeftNavigation.TestTableVisibility(instance, tableXPath);

                    LogAction("Switch to the new windows");
                    instance.SwitchTo().Window(instance.WindowHandles.Last());

                    LogAction("Maximize browser window");
                    instance.Manage().Window.Maximize();

                    LogAction("Wait for the loader to disappear");
                    WaitForLoader(instance);

                    Thread.Sleep(1500);

                    LogAction("Clear listing selectize");
                    instance.ExecuteJavaScript("$('#listing_selected')[0].selectize.clear();");

                    LogAction("Check if we are on the luau listing page");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='basic-info']/tbody/tr/td[1]/span"), 10).Text, Does.Contain("Luau"));

                    LogAction("Filter to see Hilton Neptun Lozenets");
                    LocateElement(instance, By.Id("listing_selected-selectized"), 10).SendKeys("нептун");

                    LogAction("Select the suggested hotel");
                    LocateElement(instance, By.Id("listing_selected-selectized"), 10).SendKeys(Keys.Enter);

                    LogAction("Go to the selected hotel's review page");
                    LocateElement(instance, By.XPath("//*[@id='submit_button']"), 10).Click();

                    LogAction("Assert we are on the right page");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[1]/div/div/div[2]/div/div[1]/h3"), 10).Text, Does.Contain("Google Reviews Management and Statistics"));

                    LogAction("Assert we are on the right listing");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='basic-info']/tbody/tr/td[1]/span"), 10).Text, Does.Contain("Нептун Лозенец"));

                    LogAction("Clear listing selectize");
                    instance.ExecuteJavaScript("$('#listing_selected')[0].selectize.clear();");

                    LogAction("Filter to see Luau");
                    LocateElement(instance, By.Id("listing_selected-selectized"), 10).SendKeys("luau");

                    LogAction("Select the suggested hotel");
                    LocateElement(instance, By.Id("listing_selected-selectized"), 10).SendKeys(Keys.Enter);

                    LogAction("Go to the selected hotel's review page");
                    LocateElement(instance, By.XPath("//*[@id='submit_button']"), 10).Click();
                    break;
            }

            LogAction("Wait for the loader to disappear");
            WaitForLoader(instance);

            LogAction("Assert we are on the right page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[1]/div/div/div[2]/div/div[1]/h3"), 10).Text, Does.Contain("Google Reviews Management and Statistics"));

            LogAction("Assert we are on the right listing");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='basic-info']/tbody/tr/td[1]/span"), 10).Text, Does.Contain("Luau"));

            //hotelcount HERE

            LogAction("Click on the reviewer filter");
            LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).Click();

            LogAction("Clear if any text in the filter");
            LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).Clear();

            LogAction("Filter reviews by Evgeni");
            LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).SendKeys("evgeni");

            LogAction("Check if Evgeni's review is found");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[3]"), 10).Text, Does.Contain("Evgeni"));

            LogAction("Clear text in filter");
            LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).Clear();

            LogAction("Filter reviews by Pavlina");
            LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).SendKeys("pavlina");

            if(userRole == "Hotel Manager" || userRole == "Director")
            {
                LogAction("Click on review to reply");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[6]/a[1]"), 10).Click();

                LogAction("Click on comment input");
                LocateElement(instance, By.Id("comment"), 10).Click();

                LogAction("Click on reply input");
                LocateElement(instance, By.Id("reply_input"), 10).Click();

                LogAction("Clear reply input");
                LocateElement(instance, By.Id("reply_input"), 10).Clear();

                LogAction("Input review reply");
                LocateElement(instance, By.Id("reply_input"), 10).SendKeys("Thank you for your rating! :)");

                LogAction("Click on submit reply button");
                LocateElement(instance, By.XPath("//*[@id='replyModal']/div[2]/div[2]/div[2]/button[2]"), 10).Click();

                LogAction("Click on yes post confirmation button");
                LocateElement(instance, By.XPath("//button[contains(.,'Yes, Post')]"), 5).Click();

                LogAction("Close green notification");
                CloseNotification(instance);

                LogAction("Clear text in filter");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).Clear();

                Thread.Sleep(2000);

                LogAction("Sort reviews by date");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[1]/input"), 10).Click();

                LogAction("Sort reviews by date");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[1]/input"), 10).Click();

                LogAction("Store review reply text");
                String reviewer = LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[3]"), 10).Text;
                String replyText = LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[5]"), 10).Text;

                LogAction("Click on delete reply button");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[6]/a[2]"), 10).Click();

                LogAction("Click on confirmation for the deletion of the reply");
                LocateElement(instance, By.XPath("//button[contains(.,'Yes, Delete it')]"), 5).Click();

                LogAction("Close green notification");
                CloseNotification(instance);

                LogAction("Confirm that there is no reply in the datatable row");
                Assert.AreEqual("", LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[5]"), 10).Text);

                LogAction("Click on review to reply");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[6]/a[1]"), 10).Click();

                LogAction("Click on comment input");
                LocateElement(instance, By.Id("comment"), 10).Click();

                LogAction("Click on reply input");
                LocateElement(instance, By.Id("reply_input"), 10).Click();

                LogAction("Clear reply input");
                LocateElement(instance, By.Id("reply_input"), 10).Clear();

                LogAction("Input review reply");
                LocateElement(instance, By.Id("reply_input"), 10).SendKeys(replyText);

                LogAction("Click on submit reply button");
                LocateElement(instance, By.XPath("//*[@id='replyModal']/div[2]/div[2]/div[2]/button[2]"), 10).Click();

                LogAction("Click on yes post confirmation button");
                LocateElement(instance, By.XPath("//button[contains(.,'Yes, Post')]"), 5).Click();

                LogAction("Close green notification");
                CloseNotification(instance);

                Thread.Sleep(15000);

                LogAction("Get current URL and save it to a variable");
                String currentURL = instance.Url;

                nonLuau.checkReviewGoogle(instance, reviewer, replyText);

                LogAction("Go back to the saved url");
                instance.Navigate().GoToUrl(currentURL);

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance);

                LogAction("Sort reviews by response");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[5]/input"), 10).Click();

                LogAction("Store the first reviews text");
                String emptyReview = LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[5]"), 10).Text;

                LogAction("Check to see if it is empty");
                Assert.AreEqual(emptyReview, "");

                LogAction("Sort reviews by date");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[1]/input"), 10).Click();

                LogAction("Clear text in filter");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[1]/input"), 10).Clear();

                LogAction("Store the first date");
                String firstDate = LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[1]"), 10).Text;

                LogAction("Store the nth date");
                String nthDate = LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[9]/td[1]"), 10).Text;

                LogAction("Check if the first date is smaller");
                if (String.Compare(firstDate, nthDate) < 0)
                {
                    Assert.True(true);
                }
                else
                {
                    Assert.True(false);
                }

                LogAction("Clear reviewer input field");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).Clear();

                LogAction("Filter reviews by Pavlina");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/thead/tr/th[3]/input"), 10).SendKeys("pavlina");

                LogAction("Click on delete reply button");
                LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[6]/a[2]"), 10).Click();

                LogAction("Click on confirmation for the deletion of the reply");
                LocateElement(instance, By.XPath("//button[contains(.,'Yes, Delete it')]"), 10).Click();

                LogAction("Close green notification");
                CloseNotification(instance);

                LogAction("Confirm that there is no reply in the datatable row");
                Assert.AreEqual("", LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[5]"), 10).Text);
            }
            else
            {
                try
                {
                    LogAction("Click on review to reply");
                    LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr/td[6]/a[1]"), 10).Click();
                    Assert.IsTrue(false);
                }
                catch
                {
                    Console.WriteLine("Cant reply to reviews");
                }
            } 
        }

        public static void UserDetails(IWebDriver instance, int hotelCount, string email)
        {
            string passwordOld = ConfigurationManager.AppSettings["password"];
            string passwordNew = ConfigurationManager.AppSettings["passwordNew"];

            LogAction("Click on the profile picture icon");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10).Click();

            try
            {
                LogAction("Click on my profile");
                LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10).Click();
            }
            catch
            {
                LogAction("Click on my profile 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Check if the profile to edit is the same one");
            //Assert.AreEqual("richard.cridford@hilton.com", LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[3]/div[2]/span[1]/a"), 10).Text);

            LogAction("Click on edit button to go in edit mode");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Clear first name field value");
            LocateElement(instance, By.Id("first_name"), 10).Clear();

            LogAction("Input text in the first name field");
            LocateElement(instance, By.Id("first_name"), 10).SendKeys("Test");

            LogAction("Click on the save profile button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LogAction("Check that the name has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[1]/div/span[1]"), 10).Text, Does.Contain("Test"));

            LogAction("Click on the edit profile button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Clear last name field");
            LocateElement(instance, By.Id("last_name"), 10).Clear();

            LogAction("Input text in the last name field");
            LocateElement(instance, By.Id("last_name"), 10).SendKeys("Test2");

            LogAction("Click on the save profile button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LogAction("Check that the name has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[2]/div/span[1]"), 10).Text, Does.Contain("Test2"));

            LogAction("Click on the edit profile button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Clear position field");
            LocateElement(instance, By.Id("position"), 10).Clear();

            LogAction("Input text in the position field");
            LocateElement(instance, By.Id("position"), 10).SendKeys("Client");

            LogAction("Click on the save profile button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LogAction("Check that the position has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[3]/div/span[1]"), 10).Text, Does.Contain("Client"));

            LogAction("Click on the edit profile button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Clear first name field value");
            LocateElement(instance, By.Id("first_name"), 10).Clear();

            LogAction("Input text in the first name field");
            LocateElement(instance, By.Id("first_name"), 10).SendKeys("Test3");

            LogAction("Clear last name field");
            LocateElement(instance, By.Id("last_name"), 10).Clear();

            LogAction("Input text in the first name field");
            LocateElement(instance, By.Id("last_name"), 10).SendKeys("Test4");

            LogAction("Clear last name field");
            LocateElement(instance, By.Id("position"), 10).Clear();

            LogAction("Input text in the first name field");
            LocateElement(instance, By.Id("position"), 10).SendKeys("Client1");

            LogAction("Click on the save profile button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LogAction("Check that the first name has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[1]/div/span[1]"), 10).Text, Does.Contain("Test3"));

            LogAction("Check that the last name has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[2]/div/span[1]"), 10).Text, Does.Contain("Test4"));

            LogAction("Check that the position has changed");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='user_profile_tabs_content']/li[1]/div/div/ul/li[3]/div/span[1]"), 10).Text, Does.Contain("Client1"));

            LogAction("Click on the edit profile button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click on the change password button");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs_content']/li[1]/div/div[1]/div[6]/button"), 10).Click();

            LogAction("Input text in the old password field");
            LocateElement(instance, By.Id("password_old"), 10).SendKeys(passwordOld);

            LogAction("Input text in the new password field");
            LocateElement(instance, By.Id("password"), 10).SendKeys(passwordNew);

            LogAction("Input text in the repeat new password field");
            LocateElement(instance, By.Id("password_repeat"), 10).SendKeys(passwordNew);

            LogAction("Click on the change password button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/div/div/div/div[2]/form/div[4]/button"), 10).Click();

            LogAction("Click on the profile picture icon");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10).Click();

            LogAction("Click on the logout button");
            LocateElement(instance, By.LinkText("Logout"), 10).Click();

            try
            {
                LogAction("Input correct email address");
                LocateElement(instance, By.Id("inputEmail"), 2).Clear();
                LocateElement(instance, By.Id("inputEmail"), 2).SendKeys(email);
            }
            catch
            {
                LogAction("Click on my profile 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Input correct email address");
            LocateElement(instance, By.Id("inputEmail"), 2).Clear();
            LocateElement(instance, By.Id("inputEmail"), 2).SendKeys(email);

            LogAction("Input password");
            LocateElement(instance, By.Id("inputPassword"), 2).SendKeys(passwordNew);

            LogAction("Click Sign in Button");
            LocateElement(instance, By.CssSelector("#login_form > div.uk-margin-medium-top > button"), 2).Click();

            LogAction("Click on the profile picture icon");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10).Click();

            try
            {
                LogAction("Click on my profile");
                LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10).Click();
            }
            catch
            {
                LogAction("Click on my profile 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Click on the edit profile button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click on the change password button");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs_content']/li[1]/div/div[1]/div[6]/button"), 10).Click();

            LogAction("Input text in the old password field");
            LocateElement(instance, By.Id("password_old"), 10).SendKeys(passwordNew);

            LogAction("Input text in the new password field");
            LocateElement(instance, By.Id("password"), 10).SendKeys(passwordOld);

            LogAction("Input text in the repeat new password field");
            LocateElement(instance, By.Id("password_repeat"), 10).SendKeys(passwordOld);

            LogAction("Click on the change password button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/div/div/div/div[2]/form/div[4]/button"), 10).Click();

            LogAction("Click on the save profile button");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click on the Business Access tab");
            LocateElement(instance, By.XPath("//*[@id='user_profile_tabs']/li[2]/a"), 10).Click();

            try
            {
                LogAction("Click on the luau listing to its review management page");
                LocateElement(instance, By.XPath("//*[@id='businessAccessList']/li/div[2]/div/span[1]/a"), 10).Click();
            }
            catch
            {
                LogAction("Click on the Business Access tab 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='user_profile_tabs']/li[2]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='businessAccessList']/li/div[2]/div/span[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Click on the profile picture icon");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10).Click();

            try
            {
                LogAction("Click on my profile");
                LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10).Click();
            }
            catch
            {
                LogAction("Click on my profile 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                webElement = LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[3]/div/ul/li[1]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);
            }

            LogAction("Click on Security tab");
            LocateElement(instance, By.XPath("//*[@id='user_profile_tabs']/li[3]/a"), 10).Click();

            Thread.Sleep(4000);

            LogAction("Click on the enable authenticator button");

            LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Disable authenticator'])[1]/following::button[1]"), 10).Click();
            Thread.Sleep(1000);

            try
            {
                LogAction("Check if the modal show what it has to");
                Assert.AreEqual("Enable Authenticator Support", LocateElement(instance, By.XPath("//*[@id='enableAuthenticator']/div/div[1]/h3"), 10).Text);
            }
            catch
            {
                LogAction("Click on Security tab 2nd attempt");

                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='user_profile_tabs']/li[3]/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the enable authenticator button 2nd attempt");
                webElement = LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Disable authenticator'])[1]/following::button[1]"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Check if the modal show what it has to");
                Assert.AreEqual("Enable Authenticator Support", LocateElement(instance, By.XPath("//*[@id='enableAuthenticator']/div/div[1]/h3"), 10).Text);
            }

            Thread.Sleep(3000);

            LogAction("Close the authenticator modal");
            LocateElement(instance, By.XPath("//*[@id='enableAuthenticator']/div/div[1]/div/button"), 10).Click();
            Thread.Sleep(2500);

            LogAction("Click on the ask for help button");
            LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[2]/a/i"), 10).Click();

            LogAction("Click on subject field");
            LocateElement(instance, By.Id("subject"), 10).Clear();

            LogAction("Input text in the subject field");
            LocateElement(instance, By.Id("subject"), 10).SendKeys("Help");

            LogAction("Click on the question field");
            LocateElement(instance, By.Id("question"), 10).Clear();

            LogAction("Input text in the question field");
            LocateElement(instance, By.Id("question"), 10).SendKeys("Please assist with review management");

            LogAction("Click on the submit question button");
            LocateElement(instance, By.Id("send_help_email"), 10).Click();

            LogAction("Click OK on the confirmation modal");
            LocateElement(instance, By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[2]/following::button[1]"), 10).Click();
            Thread.Sleep(6000);
            
        }

        public static void CustomRoleIssueTest(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            string password = ConfigurationManager.AppSettings["password"];

            LogAction("Verify that we are on the my businesses page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='wrapper']/div[1]/div/div/div[1]/h3"), 20).Text, Does.Contain("My Businesses"));

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the user management option");
            LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

            try
            {
                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the gmb reporting option in the sidebar with JS");
                webElement = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            LogAction("Filter table by email");
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).SendKeys("desislava");

            LogAction("Click on the result in the table");
            LocateElement(instance, By.XPath("//*[@id='users-table']/tbody/tr[1]/td[10]/div/div/a[1]"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Maximize browser window");
            instance.Manage().Window.Maximize();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Click on edit button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click on roles and permissions tab");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[4]/a"), 10).Click();

            LogAction("Click on the selected role");
            LocateElement(instance, By.XPath("//*[@id='permissions']/div[6]/div/div/div/div/p[2]/label"), 10).Click();

            LogAction("Save changes to the profile");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LoginPage.Logout(instance);

            LoginPage.Login(instance, "desislava@luaugroup.com");

            try
            {
                LogAction("Wait for the loader to disappear");
                WaitForLoader(instance);

                LogAction("Click on Tasks");
                LocateElement(instance, By.XPath("//*[@id='project_tab']/a"), 10).Click();

                LogAction("Wait for the loader to disappear");
                WaitForLoader(instance);

                LogAction("Check that there are tasks in the table");
                Assert.AreNotEqual(LocateElement(instance, By.XPath("//*[@id='tasks-table_info']"), 20).Text, "Showing 0 to 0 of 0 entries");
            }
            catch
            {
                Console.WriteLine("There was error with the custom role");
            }

            LoginPage.Logout(instance);

            LoginPage.Login(instance, ConfigurationManager.AppSettings["masterEmail"]);

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the user management option");
            LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

            try
            {
                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the reporting menu in the sidebar with JS");
                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the gmb reporting option in the sidebar with JS");
                webElement = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            LogAction("Filter table by email");
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).SendKeys("desislava");

            LogAction("Click on the result in the table");
            LocateElement(instance, By.XPath("//*[@id='users-table']/tbody/tr[1]/td[10]/div/div/a[1]"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Maximize browser window");
            instance.Manage().Window.Maximize();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Click on edit button");
            LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

            LogAction("Click on roles and permissions tab");
            LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[4]/a"), 10).Click();

            LogAction("Click on the selected role");
            LocateElement(instance, By.XPath("//*[@id='roles']/p[2]/label"), 10).Click();

            LogAction("Save changes to the profile");
            LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();

            LoginPage.Logout(instance);

        }

    }
}
