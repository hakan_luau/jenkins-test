﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class LeftNavigation : HelpFunctions
    {
        public static IWebDriver driver;

        public static string baseURL = ConfigurationManager.AppSettings["url"];

        /*public static IDictionary<string, string> dict = new Dictionary<string, string> {
            { "dashboard", "//*[@id='dashboard_tab']" },
            { "tasks", "//*[@id='project_tab']" },
            { "businesses", "//*[@id='businesses_tab']" },
            { "reviewManagement", "//*[@id='reviews_tab']" },
            { "reporting", "//*[@id='reporting_tab']" },
            { "reportingGMB", "//*[@id='gmb_reporting_tab']" },
            { "reportingGooglePlus", "//*[@id='gplus_reporting_tab']" },
            { "reportingReviews", "//*[@id='reviews_reporting_tab']" },
            { "citations", "//*[@id='citations_tab']" },
            { "citationsDirectories", "//*[@id='directories_citations_tab']" },
            { "citationsActions", "//*[@id='actions_citations_tab']" },
            { "citationsEmailTemplates", "//*[@id='templates_citations_tab']" },
            { "knowledgeBase", "//*[@id='kb_tab']" },
            { "knowledgeBaseProcessMaps", "//*[@id='processes_kb_tab']" },
            { "knowledgeBaseCompanyDocuments", "//*[@id='company_documents_tab']" },
            { "knowledgeBaseTrainingMaterials", "//*[@id='training_materials_tab']" },
            { "knowledgeBaseClientsDocuments", "//*[@id='clients_documents_tab']" },
            { "humanResources", "//*[@id='hr_tab']" },
            { "administration", "//*[@id='admin_tab']" },
            { "administrationUserManagement", "//*[@id='users_admin_tab']" },
            { "administrationClientManagement", "//*[@id='clients_admin_tab']" },
            { "administrationSecurityAndPermission", "//*[@id='security_permission_tab']" },
        };*/

        public static Dictionary<string, string[]> linkArray = new Dictionary<string, string[]>{
            /*{ "view Dashboard", new string[] {
                baseURL + "/dashboard",
                "",
                "//*[@id='permissions']/div[1]/div/div/div/div/p/label",
                "",
                "//*[@id='dashboard_tab']"
                
            } },
            { "view Tasks", new string[] {
                baseURL + "/tasks",
                "",
                "//*[@id='permissions']/div[2]/div/div/div/div/p/label",
                "",
                "//*[@id='project_tab']"
            } },
            { "view Listings", new string[] {
                baseURL + "/business",
                "",
                "//*[@id='permissions']/div[3]/div/div/div/div/p/label",
                "",
                "//*[@id='businesses_tab']"

            } },
            { "view Client Templates", new string[] {
                baseURL + "/messaging/client-templates",
                "//*[@id='permissions']/div[4]/div/div/div/div/p[1]/label",
                "//*[@id='permissions']/div[4]/div/div/div/div/p[6]/label",
                "//*[@id='messaging_tab']/a",
                "//*[@id='templates_tab']/a"
            } },
            { "edit Client Templates", new string[] {
                baseURL + "/messaging/client-templates",
                "",
                "//*[@id='permissions']/div[4]/div/div/div/div/p[7]/label",
                "//*[@id='messaging_tab']/a",
                "//*[@id='templates_tab']/a"
            } },
            { "view Review Management", new string[] {
                baseURL + "/reviews",
                "//*[@id='permissions']/div[5]/div/div/div/div/p[1]/label",
                "//*[@id='permissions']/div[5]/div/div/div/div/p[2]/label",
                "",
                "//*[@id='reviews_tab']"
            } },
            { "edit Review Management", new string[] {
                baseURL + "/reviews",
                "",
                "//*[@id='permissions']/div[5]/div/div/div/div/p[3]/label",
                "",
                "//*[@id='reviews_tab']"
            } },
            { "view GMB", new string[] {
                baseURL + "/reports/gmb",
                "",
                "//*[@id='permissions']/div[6]/div/div/div/div/p[2]/label",
                "//*[@id='reporting_tab']",
                "//*[@id='gmb_reporting_tab']"
                
            } },
            { "view Google+", new string[] {
                baseURL + "/reports/gplus",
                "",
                "//*[@id='permissions']/div[6]/div/div/div/div/p[3]/label",
                "//*[@id='reporting_tab']",
                "//*[@id='gplus_reporting_tab']"
                
            } },
            { "view Revenue", new string[] {
                baseURL + "/reports/revenue",
                "",
                "//*[@id='permissions']/div[6]/div/div/div/div/p[4]/label",
                "//*[@id='reporting_tab']",
                "//*[@id='revenue_reporting_tab']/a"
            } },
            { "view Rankings", new string[] {
                baseURL + "/reports/rankings",
                "",
                "//*[@id='permissions']/div[6]/div/div/div/div/p[5]/label",
                "//*[@id='reporting_tab']",
                "//*[@id='rankings_reporting_tab']/a"
            } },
            { "view Reviews", new string[] {
                baseURL + "/reports/reviews",
                "",
                "//*[@id='permissions']/div[6]/div/div/div/div/p[6]/label",
                "//*[@id='reporting_tab']",
                "//*[@id='reviews_reporting_tab']"
            } },
            { "view Directories", new string[] {
                baseURL + "/citations/directories",
                "",
                "//*[@id='permissions']/div[7]/div/div/div/div/p[2]/label",
                "//*[@id='citations_tab']",
                "//*[@id='directories_citations_tab']"
            } },
            { "edit Directories", new string[] {
                baseURL + "/citations/directories",
                "",
                "//*[@id='permissions']/div[7]/div/div/div/div/p[6]/label",
                "//*[@id='citations_tab']",
                "//*[@id='directories_citations_tab']"
            } },
            { "view Actions", new string[] {
                baseURL + "/citations/actions",
                "",
                "//*[@id='permissions']/div[7]/div/div/div/div/p[3]/label",
                "//*[@id='citations_tab']",
                "//*[@id='actions_citations_tab']"
            } },
            { "view Email Templates", new string[] {
                baseURL + "/citations/templates",
                "",
                "//*[@id='permissions']/div[7]/div/div/div/div/p[4]/label",
                "//*[@id='citations_tab']",
                "//*[@id='templates_citations_tab']"
            } },*/
            { "view Process Maps", new string[] {
                baseURL + "/kb/processes",
                "",
                "//*[@id='permissions']/div[8]/div/div/div/div/p[2]/label",
                "//*[@id='kb_tab']",
                "//*[@id='processes_kb_tab']"
            } },
            { "view Company Documents", new string[] {
                baseURL + "/kb/company",
                "",
                "//*[@id='permissions']/div[8]/div/div/div/div/p[3]/label",
                "//*[@id='kb_tab']",
                "//*[@id='company_documents_tab']"
            } },
            { "view Training Materials", new string[] {
                baseURL + "/kb/trainings",
                "",
                "//*[@id='permissions']/div[8]/div/div/div/div/p[4]/label",
                "//*[@id='kb_tab']",
                "//*[@id='training_materials_tab']"
            } },
            { "view Clients documents", new string[] {
                baseURL + "/kb/clients",
                "",
                "//*[@id='permissions']/div[8]/div/div/div/div/p[5]/label",
                "//*[@id='kb_tab']",
                "//*[@id='clients_documents_tab']"
            } },
            /*{ "view Human Resources", new string[] {
                baseURL + "/hr",
                "",
                "//*[@id='permissions']/div[9]/div/div/div/div/p[1]/label",
                "",
                "//*[@id='hr_tab']"
            } },*/
            { "view Keywords Management", new string[] {
                baseURL + "/admin/keywords",
                "",
                "//*[@id='permissions']/div[10]/div/div/div/div/p[8]/label",
                "//*[@id='admin_tab']",
                "//*[@id='keywords_admin_tab']/a"
            } },
            { "edit Keywords Management", new string[] {
                baseURL + "/admin/keywords",
                "",
                "//*[@id='permissions']/div[10]/div/div/div/div/p[9]/label",
                "//*[@id='admin_tab']",
                "//*[@id='keywords_admin_tab']/a"
            } },
            { "view User Management", new string[] {
                baseURL + "/admin/users",
                "",
                "//*[@id='permissions']/div[10]/div/div/div/div/p[2]/label",
                "//*[@id='admin_tab']",
                "//*[@id='users_admin_tab']"
            } },
            { "view Security & Permissions", new string[] {
                baseURL + "/admin/security",
                "",
                "//*[@id='permissions']/div[10]/div/div/div/div/p[4]/label",
                "//*[@id='admin_tab']",
                "//*[@id='security_permission_tab']"
            } },
            { "detailedSearch", new string[] {
                baseURL + "/search",
                "",
                "//*[@id='permissions']/div[11]/div/div/div/div/p/label",
                "",
                "//*[@id='header_main']/div[1]/nav/div/ul/li[2]/a"
            } }
        };

        public static void TestRemovingPermissions(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            string password = ConfigurationManager.AppSettings["password"];

            LogAction("Verify that we are on the my businesses page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='wrapper']/div[1]/div/div/div[1]/h3"), 20).Text, Does.Contain("My Businesses"));

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the user management option");
            LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            try
            {
                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the admin menu in the sidebar with JS");
                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the user management option in the sidebar with JS");
                webElement = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            LogAction("Fitler by username admin");
            LocateElement(instance, By.XPath("//*[@id='users-table_filter']/div/input"), 10).SendKeys("admin@luaugroup.com");

            LogAction("Go to the first result profile");
            LocateElement(instance, By.XPath("//*[@id='users-table']/tbody/tr/td[10]/div/div/a[1]"), 10).Click();

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Get current URL to not navigate multiple times to it");
            String currentURL = instance.Url;

            foreach (var kvp in linkArray)
            {
                string key = kvp.Key;
                string[] val = kvp.Value;

                Console.WriteLine(key);

                LoginPage.Logout(instance);

                LoginPage.Login(instance, ConfigurationManager.AppSettings["masterEmail"]);

                LogAction("Go to the profile's url");
                instance.Navigate().GoToUrl(currentURL);

                LogAction("Click on edit profile button");
                LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

                LogAction("Click on roles and permissions");
                LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[3]/a"), 5).Click();

                try
                {
                    LogAction("Verify that were in the roles and permissions tab");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='permissions']/h3"), 20).Text, Does.Contain("Permissions"));
                }
                catch
                {
                    LogAction("Click on roles and permissions 2nd attempt");
                    LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[4]/a"), 5).Click();
                }

                LogAction("Click on child permission");
                LocateElement(instance, By.XPath(val[2]), 10).Click();

                IWebElement element = LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10);
                element.Submit();

                LoginPage.Logout(instance);

                LoginPage.Login(instance, "custom@hilton.com");

                if (val[3] != "")
                {
                    LogAction("Click on dropdown menu in sidebar");
                    LocateElement(instance, By.XPath(val[3]), 10).Click();
                }
                Thread.Sleep(500);

                LogAction("Check if element is visible");
                if (IsElementClickable(instance, By.XPath(val[4])))
                {
                    Console.WriteLine(By.XPath(val[4]));
                    Console.WriteLine("Exists");
                }
                else
                {
                    Console.WriteLine(By.XPath(val[4]));
                    Console.WriteLine("Does not exist");
                }
            }
        }

        public static void TestSidebarLink(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            string password = ConfigurationManager.AppSettings["password"];

            LogAction("Verify that we are on the my businesses page");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='wrapper']/div[1]/div/div/div[1]/h3"), 20).Text, Does.Contain("My Businesses"));

            LogAction("Click on the administration section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the user management option");
            LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10).Click();

            try
            {
                LogAction("Verify that were in the user management section");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }
            catch
            {
                LogAction("Click on the admin menu in the sidebar with JS");
                IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Click on the user management option in the sidebar with JS");
                webElement = LocateElement(instance, By.XPath("//*[@id='users_admin_tab']/a"), 10);
                instance.ExecuteJavaScript("arguments[0].click();", webElement);

                LogAction("Verify that were in the user management section 2nd attempt");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='userManagement']/div[1]/h3"), 20).Text, Does.Contain("User Management"));
            }

            LogAction("Click to filter user");
            LocateElement(instance, By.XPath("//*[@id='users-table']/thead/tr/th[3]/div/input"), 10).SendKeys("custom");

            LogAction("Click on the edit profile button of the result");
            LocateElement(instance, By.XPath("//*[@id='users-table']/tbody/tr/td[10]/div/div/a[1]"), 10).Click();

            //*[@id="users-table"]/thead/tr/th[1]/div/input

            LogAction("Switch to the new windows");
            instance.SwitchTo().Window(instance.WindowHandles.Last());

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance);

            LogAction("Get current URL to not navigate multiple times to it");
            String currentURL = instance.Url;

            foreach (var kvp in linkArray)
            {
                string key = kvp.Key;
                string[] val = kvp.Value;

                Console.WriteLine(key);

                LoginPage.Logout(instance);

                LoginPage.Login(instance, ConfigurationManager.AppSettings["masterEmail"]);

                LogAction("Go to the profile's url");
                instance.Navigate().GoToUrl(currentURL);

                LogAction("Click on edit profile button");
                LocateElement(instance, By.XPath("//*[@id='user_profile']/div/div/div[1]/a"), 10).Click();

                LogAction("Click on roles and permissions");
                LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[3]/a"), 5).Click();

                try
                {
                    LogAction("Verify that were in the roles and permissions tab");
                    Assert.That(LocateElement(instance, By.XPath("//*[@id='permissions']/h3"), 20).Text, Does.Contain("Permissions"));
                }
                catch
                {
                    LogAction("Click on roles and permissions 2nd attempt");
                    LocateElement(instance, By.XPath("//*[@id='user_edit_tabs']/li[4]/a"), 5).Click();
                }

                /*IWebElement checkBox1 = LocateElement(instance, By.XPath("//*[@id='permissions']/div[5]/div/div/div/div/p[1]/div/ins"), 10);

                if (!checkBox1.Selected)
                {
                    Console.WriteLine("Reporting is not selected");
                }
                else Console.WriteLine("Reporting is selected");*/

                if (val[1] != "")
                {
                    LogAction("Click on parent permission");
                    LocateElement(instance, By.XPath(val[1]), 10).Click();
                }
                LogAction("Click on child permission");
                LocateElement(instance, By.XPath(val[2]), 10).Click();

                IWebElement element = LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10);
                element.Submit();

                /*LogAction("Click on save profile button");
                LocateElement(instance, By.XPath("//*[@id='page_content_inner']/form/div/div[1]/div/div[1]/div/div[4]/button"), 10).Click();*/

                LoginPage.Logout(instance);

                LoginPage.Login(instance, "custom@hilton.com");

                Thread.Sleep(3000);

                /*LogAction("Wait for loader to disappear");
                WaitForLoader(instance);*/

                if (val[3] != "")
                {
                    LogAction("Click on dropdown menu in sidebar");
                    LocateElement(instance, By.XPath(val[3]), 10).Click();
                }
                Thread.Sleep(500);

                LogAction("Click on button in the sidebar");
                LocateElement(instance, By.XPath(val[4]), 10).Click();

                switch (key)
                {
                    case "edit Client Templates":
                        LogAction("Click on add button");
                        LocateElement(instance, By.XPath("//*[@id='addTemplateWrapper']/a"), 10).Click();

                        LogAction("Close the modal");
                        LocateElement(instance, By.XPath("//*[@id='template_add_form']/div[2]/div[2]/button[2]"), 10).Click();

                        Thread.Sleep(1000);
                        break;
                    case "edit Single Review Management":
                        try
                        {
                            LogAction("Click on the first hotels more info button");
                            LocateElement(instance, By.XPath("//*[@id='active-table']/tbody/tr[1]/td[8]/a/button"), 10).Click();

                            LogAction("Switch to the new windows");
                            instance.SwitchTo().Window(instance.WindowHandles.Last());

                            LogAction("Wait for loader to disappear");
                            WaitForLoader(instance);

                            LogAction("Click to reply on the first review");
                            LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[6]/a[1]"), 10).Click();

                            LogAction("Close the modal");
                            LocateElement(instance, By.XPath("//*[@id='replyModal']/div[2]/div[2]/div[2]/button[1]"), 10).Click();

                            Thread.Sleep(1000);
                        }
                        catch
                        {
                            LogAction("Click to reply on the first review");
                            LocateElement(instance, By.XPath("//*[@id='reviews-table']/tbody/tr[1]/td[6]/a[1]"), 10).Click();

                            LogAction("Close the modal");
                            LocateElement(instance, By.XPath("//*[@id='replyModal']/div[2]/div[2]/div[2]/button[1]"), 10).Click();

                            Thread.Sleep(1000);
                        }
                        break;
                    case "edit Directory":
                        LogAction("Click on button in the sidebar");
                        LocateElement(instance, By.XPath("//*[@id='directory_add_wrapper']/a"), 10).Click();

                        LogAction("Close the modal");
                        LocateElement(instance, By.XPath("//*[@id='directory_modal']/div/div/div[2]/button[2]"), 10).Click();

                        Thread.Sleep(1000);
                        break;
                    case "edit Keywords Management":
                        LogAction("Wait for loader to disappear");
                        WaitForLoader(instance);

                        LogAction("Click on the combinations tab");
                        LocateElement(instance, By.XPath("//*[@id='combinations_link']/a"), 10).Click();

                        LogAction("Wait for loader to disappear");
                        WaitForLoader(instance);

                        LogAction("Click on the composite button in the sidebar");
                        LocateElement(instance, By.XPath("//*[@id='combinations_add_wrapper']/a"), 10).Click();
                        break;
                }
            }
        }

        public static void TestTableVisibility(IWebDriver instance, string tableXPath)
        {
            /*LogAction("Get table element");
            IWebElement table = instance.FindElement(By.XPath(tableXPath));

            LogAction("Split table to table cells");
            IList<IWebElement> rows_table = table.FindElements(By.TagName("td"));

            LogAction("Go through every table cell");
            foreach (var item in rows_table)
            {
                //Console.WriteLine(item.Text.ToString());
                if (item.Text.ToString() == "")
                {
                    Console.WriteLine("There is empty <td>");
                    Assert.IsTrue(false);
                }
            }*/
        }

        public static void CheckBusinessCount(IWebDriver instance)
        {
            Dictionary<string, string[]> tablesCheck = new Dictionary<string, string[]>{
                { "businesses", new string[] {
                    "/business",
                    "//*[@id='businesses-table_info']"
                } },
                { "reportingGMB", new string[] {
                    "/reports/gmb",
                    "//*[@id='reporting-gmb_info']"
                } },
                { "reportingGplus", new string[] {
                    "/reports/gplus",
                    "//*[@id='tech-table_info']"
                } },
                { "reportingRankings", new string[] {
                    "/reports/rankings",
                    "//*[@id='reporting-rankings_info']"
                } },
                { "reportingReviews", new string[] {
                    "/reports/reviews",
                    "//*[@id='reporting-table_info']"
                } },
                { "reviewManagement", new string[] {
                    "/reviews",
                    "//*[@id='active-table_info']"
                } }
            };

            string hotelInfo = "";
            foreach (var table in tablesCheck)//iterate over dictionary
            {
                LogAction("navigate to the url");
                instance.Navigate().GoToUrl(ConfigurationManager.AppSettings["url"] + table.Value[0]);

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 120);

                if (hotelInfo != "")
                {
                    Assert.That(LocateElement(instance, By.XPath(table.Value[1]), 20).Text, Does.Contain(hotelInfo));
                }
                else
                {
                    hotelInfo = LocateElement(instance, By.XPath(table.Value[1]), 20).Text;
                }
            }
            
        }
 
    }
}



