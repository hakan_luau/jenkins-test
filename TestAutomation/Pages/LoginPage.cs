﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Configuration;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class LoginPage : HelpFunctions
    {
        private static string loginPass = ConfigurationManager.AppSettings["password"];

        public static void Login(IWebDriver instance, string loginEmail)
        {
            LogAction("Input correct email address");
            LocateElement(instance, By.Id("inputEmail"), 2).SendKeys(loginEmail);

            LogAction("Input password");
            LocateElement(instance, By.Id("inputPassword"), 2).SendKeys(loginPass);

            LogAction("Click Sign in Button");
            LocateElement(instance, By.CssSelector("#login_form > div.uk-margin-medium-top > button"), 2).Click();
        }

        public static void Logout(IWebDriver instance)
        {
            string baseURL = ConfigurationManager.AppSettings["url"];

            LogAction("Logout manually");
            instance.Navigate().GoToUrl(baseURL + "/logout");
        }

        public static void ResetPassword(IWebDriver instance)
        {
            LogAction("Click on Need Help Button");
            LocateElement(instance, By.XPath("//*[@id='login_help_show']"), 2).Click();

            LogAction("Click on Reset Password link");
            LocateElement(instance, By.LinkText("reset your password"), 5).Click();

            LogAction("Fill the email field");
            var resetButton = LocateElement(instance, By.Id("masked_email"), 15);
            resetButton.SendKeys("hakan@luaugroup.com");

            LogAction("Click on the confirmation button");
            LocateElement(instance, By.Id("reset_pass_button"), 2).Click();

            LogAction("Wait for the green notification");
            IWebElement notificationText = LocateElement(instance, By.XPath("/html/body/div[4]/div/div"), 2);

            LogAction("Check if the modal message is success");
            Assert.IsTrue(notificationText.Text.Contains("We have sent you an email to continue with the password reset"));
        }
    }
}