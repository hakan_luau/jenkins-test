﻿using OpenQA.Selenium;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class MyBusinessesPage
    {
        public static IWebDriver driver;

        public static void NavigateToBusiness(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the detailed search icon");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='header_main']/div[1]/nav/div/ul/li[2]/a/i"), 5).Click();

            HelpFunctions.LogAction("Filter by hotel name");
            HelpFunctions.LocateElement(instance, By.Name("companyname"), 4).SendKeys("Hilton Oswiecim");

            HelpFunctions.LogAction("Click on the submit");
            HelpFunctions.LocateElement(instance, By.XPath("//button[contains(text(), 'Search')]"), 4).Click();

            HelpFunctions.LogAction("Click on first result");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='directories-table']/tbody/tr[1]/td[1]/a"), 5).Click();

        }

       /* public static void CreateListing(IWebDriver instance)
        {
            LeftNavigation.OpenBusinesses(instance);
            HelpFunctions.LogAction("Click on the create button in the bottom right corner");
            HelpFunctions.LocateElement(instance, By.XPath("//i[contains(.,'add')]"), 5).Click();

            HelpFunctions.LogAction("Write some hotel name");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='business_add_name']"), 2).SendKeys("Hilton Oswiecim");

            HelpFunctions.LogAction("Click on the Select");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='business_add_form']/div[2]/div[2]/div/div[1]"), 2).Click();

            HelpFunctions.LogAction("Chose a campaign from the dropdown");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='business_add_form']/div[2]/div[2]/div/div[2]/div/div[2]"), 2).Click();

            HelpFunctions.LogAction("Click on the Add Business Button");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='save_business']"), 2).Click();

            HelpFunctions.LogAction("go to details in Business View");
            HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[1]/div[1]/div/div/div[2]/div/div/div/div/ul/li[3]/a"), 15).Click();
        
        }*/

        public static void TestInfoCard(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the infocard button");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='overview_tab']/div[2]/div[12]/div/a/div/span"), 25).Click();

            HelpFunctions.LogAction("Click on the infocard button");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='listingViewTopToolbar']/div/div/h3/a"), 25).Click();

            HelpFunctions.LogAction("Click on the copy button");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='infocard_adobe']/button"), 7).Click();

            HelpFunctions.LogAction("Close the green notification");
            HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[12]/div/div"), 7).Click();
            
            HelpFunctions.LogAction("Close Modal");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='infocard_modal']/div/div[2]/button"), 7).Click();
        }

        public static void TestAdministration(IWebDriver instance)
        {
            Thread.Sleep(2000);
            HelpFunctions.LogAction("Click Administration Tab");
            HelpFunctions.LocateElement(instance, By.ClassName("administration"), 7).Click();
            HelpFunctions.LogAction("Click Selectize");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[1]"), 6).Click();
            HelpFunctions.LogAction("Select technician");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='administration_form']/div[1]/div[2]/div/div/div[2]/div/div[1]"), 3).Click();
            HelpFunctions.LogAction("click on switch");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='administration_form']/div[6]/div[12]/span"), 3).Click();
            Thread.Sleep(1000);
            HelpFunctions.LogAction("Click on save");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='listing_administration_save']"), 2).Click();
        }

        public static void TestDetails(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on details");
            Thread.Sleep(2000);
            HelpFunctions.LocateElement(instance, By.ClassName("details"), 7).Click();
            HelpFunctions.LogAction("Clear rooms field");
            HelpFunctions.LocateElement(instance, By.Name("rooms"), 10).Clear();
            HelpFunctions.LogAction("Input new rooms value");
            HelpFunctions.LocateElement(instance, By.Name("rooms"), 10).SendKeys("420");
            HelpFunctions.LogAction("Clear country field");
            HelpFunctions.LocateElement(instance, By.Name("country"), 10).Clear();
            HelpFunctions.LogAction("Input new country value");
            HelpFunctions.LocateElement(instance, By.Name("country"), 10).SendKeys("UAE");
            Thread.Sleep(1000);
            HelpFunctions.LogAction("Click on save");
            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='listing_details_save']"), 2).Click();

        }
    }
}
