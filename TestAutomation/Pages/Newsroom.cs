﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class Newsroom : HelpFunctions
    {
        public static void NavigateToNewsroom(IWebDriver instance)
        {
            LogAction("Click on Knowledge Base in the sidebar");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='kb_tab']/a/span[2]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click on Newsroom");
            webElement = LocateElement(instance, By.XPath("//*[@id='newsroom_tab']/a"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);
        }

        public static void viewArticles(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            try
            {
                LogAction("Click on the add article button");
                LocateElement(instance, By.XPath("//*[@id='addArticleWrapper']/a"), 10).Click();

                Assert.IsTrue(false);
            }
            catch
            {
            }

            LogAction("Click on the three dots menu for the first article");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/i"), 10).Click();

            LogAction("Click on the copy article button");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/div/ul/li/a"), 10).Click();

            LogAction("Click to check if the notification is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("copied to clipboard"));

            LogAction("Click to close the green notification modal");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div > a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Write username in the search all articles input");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[1]/div[1]/div/div/input"), 10).SendKeys("Valentina");

            LogAction("Send the input");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[1]/div[1]/div/div/input"), 10).SendKeys(Keys.Enter);

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if the first authors name is the one we input");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/ul/li[1]/a"), 10).Text, Does.Contain("Valentina"));

        }

        public static void EditNews(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Write username in the search all articles input");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[1]/div[1]/div/div/input"), 10).SendKeys("Valentina");

            LogAction("Send the input");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[1]/div[1]/div/div/input"), 10).SendKeys(Keys.Enter);

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Check if the first authors name is the one we input");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/ul/li[1]/a"), 10).Text, Does.Contain("Valentina"));

            LogAction("Click on the three dots menu for the first article");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/i"), 10).Click();

            LogAction("Click on the edit article button");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/div/ul/li[1]/a"), 10).Click();

            LogAction("Clear current URL and Input different URL");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[1]/div/input"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[1]/div/input"), 10).SendKeys("https://www.bbc.com/news/business-45894346");

            LogAction("Click on the preview button");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[2]/button"), 10).Click();

            LogAction("Wait for the website preview");
            Thread.Sleep(7000);

            LogAction("Filter Tags");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).Click();
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys("apple");

            LogAction("Select the suggested tags");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Close the dropdown");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys(Keys.Escape);

            Thread.Sleep(1500);

            string descriptionField = LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[2]/div[2]/div[2]/div/textarea"), 10).GetAttribute("value");
            if(descriptionField == "")
            {
                LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[2]/div[2]/div[2]/div/textarea"), 10).SendKeys("random description");
            }
            
            LogAction("Mark/Unmark the article as important");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[3]/div[2]/label"), 10).Click();

            LogAction("Click on the publish button");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[3]/div[2]/button"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click to check if the notification is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("Article updated."));

            LogAction("Click to close the green notification modal");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div > a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the three dots menu for the first article");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/i"), 10).Click();

            LogAction("Click on the copy article button");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/div/ul/li[3]/a"), 10).Click();
            
            LogAction("Click to check if the notification is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("copied to clipboard"));

            LogAction("Click to close the green notification modal");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div > a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the three dots menu for the first article");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/i"), 10).Click();

            Thread.Sleep(700);

            LogAction("Click on the delete article button");
            LocateElement(instance, By.XPath("//*[@id='articlesWrapper']/div/ul/li[1]/div[1]/header/div/div[2]/div[1]/div/div[2]/div/div/div/div/ul/li[2]/a"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click ok to confirm the deletion of the article");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click to check if the notification is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("successfully"));

            LogAction("Click to close the green notification modal");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div > a"), 10).Click();
        }

        public static void CreateNews(IWebDriver instance)
        {
            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Click on the add article button");
            LocateElement(instance, By.XPath("//*[@id='addArticleWrapper']/a"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Clear current URL and Input different URL");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[1]/div/input"), 10).Clear();
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[1]/div/input"), 10).SendKeys("https://www.bbc.com/news/business-45894346");

            LogAction("Click on the preview button");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[1]/div[2]/button"), 10).Click();

            LogAction("Wait for the website preview");
            Thread.Sleep(7000);

            LogAction("Filter Tags");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).Click();
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys("business");

            LogAction("Select the suggested tags");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Close the dropdown");
            LocateElement(instance, By.Id("tags_select-selectized"), 10).SendKeys(Keys.Escape);

            Thread.Sleep(1500);

            string descriptionField = LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[2]/div[2]/div[2]/div/textarea"), 10).GetAttribute("value");
            if (descriptionField == "")
            {
                LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[2]/div[2]/div[2]/div/textarea"), 10).SendKeys("descprit.random");
            }

            LogAction("Mark as important");
            LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[3]/div[2]/label"), 10).Click();

            LogAction("Click on the publish article button");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='article_add_modal']/div/div[3]/div[2]/button"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click to check if the notification is success");
            Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("The article was posted successfully"));

            LogAction("Click to close the green notification modal");
            LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div > a"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the recent tab");
            LocateElement(instance, By.XPath("//*[@id='tabs_1']/li[2]/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);

            LogAction("Click on the important tab");
            LocateElement(instance, By.XPath("//*[@id='tabs_1']/li[3]/a"), 10).Click();

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);
        }

    }
}
