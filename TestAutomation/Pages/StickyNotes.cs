﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class StickyNotes : HelpFunctions
    {
        public static void NavigateToStickyNotes(IWebDriver instance)
        {
            LogAction("Click on Messaging in the sidebar");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='messaging_tab']/a/span[2]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click on Sticky Notes");
            webElement = LocateElement(instance, By.XPath("//*[@id='notes_tab']/a"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Wait for loader to disappear");
            WaitForLoader(instance, 25);
        }
        public static void CreateNote(IWebDriver instance)
        {
            LogAction("Click on Add note");
            LocateElement(instance, By.XPath("//*[@id='newNoteCardContent']"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Input title for the node");
            LocateElement(instance, By.XPath("//*[@id='note_f_title']"), 10).SendKeys("Abra");

            LogAction("Input note content");
            LocateElement(instance, By.XPath("//*[@id='note_f_content']"), 10).SendKeys("Kadabra");

            LogAction("CLick on the colorboard");
            LocateElement(instance, By.XPath("//*[@id='note_form_template']/div[5]/div[1]/div[1]/a/i"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Select a color");
            LocateElement(instance, By.XPath("//*[@id='notes_colors_wrapper']/div/span[7]"), 10).Click();

            LogAction("Click on Add Note button");
            LocateElement(instance, By.XPath("//*[@id='note_form_template']/div[5]/div[2]/button"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click on edit labels button");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[3]/div/button"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Type label name");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[1]/div/input"), 10).SendKeys("rand");

            LogAction("Click on the colorboard");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[2]/div/a/i"), 10).Click();

            LogAction("Select a color for the label");
            LocateElement(instance, By.XPath("//*[@id='labels_colors_wrapper']/div/span[4]"), 10).Click();

            LogAction("Click on the colorboard to close it");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[2]/div/a/i"), 10).Click();

            LogAction("Click on the add button");
            LocateElement(instance, By.XPath("//*[@id='addLabelContainer']/button"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Type label name");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[1]/div/input"), 10).SendKeys("rand2");

            LogAction("Click on the colorboard");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[2]/div/a/i"), 10).Click();

            LogAction("Select a color for the label");
            LocateElement(instance, By.XPath("//*[@id='labels_colors_wrapper']/div/span[2]"), 10).Click();

            LogAction("Click on the colorboard to close it");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[1]/div[2]/div/a/i"), 10).Click();

            LogAction("Click on the add button");
            LocateElement(instance, By.XPath("//*[@id='addLabelContainer']/button"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on the save changes button");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[2]/button"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click to edit the first note");
            LocateElement(instance, By.XPath("//*[@id='notes_grid']/div/div/div[1]/a[1]/i"), 10).Click();

            Thread.Sleep(500);

            LogAction("Click to edit labels");
            LocateElement(instance, By.XPath("//*[@id='notes_grid']/div/div/div[3]/div[4]/div[2]/a/i"), 10).Click();

            LogAction("Click to select a label");
            LocateElement(instance, By.XPath("//*[@id='dropdown_labels']/div[1]/label"), 10).Click();

            LogAction("Click to close edit labels");
            LocateElement(instance, By.XPath("//*[@id='notes_grid']/div/div/div[3]/div[4]/div[2]/a/i"), 10).Click();

            LogAction("Click to save note");
            LocateElement(instance, By.XPath("//*[@id='notes_grid']/div/div/div[3]/div[5]/button[1]"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click on edit labels button");
            LocateElement(instance, By.XPath("//*[@id='vueEl']/div[3]/div/button"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click to delete the first label");
            LocateElement(instance, By.CssSelector("#labelsList > li:nth-child(1) > a > i"), 10).Click();

            LogAction("Click on the save changes button");
            LocateElement(instance, By.XPath("//*[@id='labels_modal']/div/div/div/div[2]/button"), 10).Click();

            Thread.Sleep(1500);

            LogAction("Click on the delete button of the first note");
            LocateElement(instance, By.XPath("//*[@id='notes_grid']/div[1]/div/div[1]/a[2]/i"), 10).Click();

            Thread.Sleep(1000);

            LogAction("Click ok to close notification");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 15).Click();
        }
    }
}
