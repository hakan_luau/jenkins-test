﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class TableColumns : HelpFunctions
    {
        public static void AdministrationUniversalMapping(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the brand option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the subregion option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[9]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the country option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[10]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the city option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[11]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the destination option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[12]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the number of rooms option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/ul/li[16]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='mapping-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[4]/div/div[6]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void AdministrationUserManagement(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the pagination");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='users-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[4]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void CitationsActions(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the gplus option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[10]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the fb option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[11]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the twitter option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[12]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the instagram option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[13]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the status option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[14]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the storecode option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[15]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the destination option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[16]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the region option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[17]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the tier option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/ul/li[18]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='scraper-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void CitationsDirectories(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the pagination");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='directories-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void MyBusinesses(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='businesses-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the storecode option");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='businesses-table_wrapper']/div[1]/div[1]/div/ul/li[2]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='businesses-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='businesses-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReportingGMB(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }
            
            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[7]/div/div[6]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);
            

            Thread.Sleep(5000);
        }

        public static void ReportingGPlus(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='tech-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='tech-table_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='tech-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='tech-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReportingRevenue(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-revenue_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='reporting-revenue_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }
            
            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-revenue_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-revenue_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[7]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReportingRankings(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-rankings_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='reporting-rankings_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-rankings_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-rankings_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");
            webElement = HelpFunctions.LocateElement(instance, By.CssSelector("body > div.selectize-dropdown.single.dt-selectize > div > div:nth-child(6)"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReportingReviews(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='reporting-table_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }

            HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReviewManagementMulti(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the select columns button");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='active-table_wrapper']/div[1]/div[1]/div/button[4]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            IWebElement div = instance.FindElement(By.XPath("//*[@id='active-table_wrapper']/div[1]/div[1]/div/ul"));

            LogAction("Get items of dropdown");
            IList<IWebElement> items = div.FindElements(By.ClassName("active"));
            
            LogAction("Go through every column");
            foreach (var item in items)
            {
                Console.WriteLine(item.Text);
                item.Click();
            }

            LogAction("Get items of dropdown");
            items = div.FindElements(By.TagName("li"));

            LogAction("Go through every column");
            foreach (var item in items)
            {
                item.Click();
            }
            
            HelpFunctions.LogAction("Click on the select columns button to hide the dropdown");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='active-table_length']/label"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            HelpFunctions.LocateElement(instance, By.XPath("//*[@id='active-table_previous']/a"), 10).Click();

            HelpFunctions.LogAction("Click on the pagination");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='active-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");
            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }

        public static void ReviewManagementSingle(IWebDriver instance)
        {

        }

        public static void Tasks(IWebDriver instance)
        {
            HelpFunctions.LogAction("Click on the pagination");
            IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='tasks-table_length']/label/div/div"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            HelpFunctions.LogAction("Select all");

            webElement = HelpFunctions.LocateElement(instance, By.XPath("/html/body/div[2]/div/div[6]"), 10); ///html/body/div[2]/div/div[6]
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(5000);
        }
        
    }
}

