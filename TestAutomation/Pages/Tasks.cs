﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class Tasks : HelpFunctions
    {
        public static Dictionary<string, string[]> taskAttributes = new Dictionary<string, string[]>{
            { "taskName", new string[] {
                "task_details_name",
                "Test Task",
                "Test Task2",
                "text"
            } },
            { "businesses", new string[] {
                "task_details_listings-selectized",
                "luau",
                "algarve",
                "multiselect"
            } },
            { "pils", new string[] {
                "task_details_person_in_loop-selectized",
                "diana",
                "radoslav",
                "multiselect"
            } },
            { "deadline", new string[] {
                "task_details_due_date",
                "2020-12-12",
                "2022-10-10",
                "text"
            } },
            { "status", new string[] {
                "task_details_status-selectized",
                "In Proce",
                "AW",
                "select",
                "//*[@id='task_details_tab']/div/div/div[8]/div/div/div[1]/div"
            } },
            { "tags", new string[] {
                "task_details_tags-selectized",
                "bing",
                "star",
                "multiselect"
            } },
            { "taskManager", new string[] {
                "task_details_owner-selectized",
                "pavlina",
                "desis",
                "select",
                "//*[@id='task_details_tab']/div/div/div[11]/div/div/div[1]/div"
            } },
            { "requested", new string[] {
                "task_details_requested_by",
                "John",
                "Amanda",
                "text"
            } },
            { "link", new string[] {
                "task_details_link",
                "http://localhost",
                "http://localmocal",
                "text"
            } },
            { "description", new string[] {
                "task_details_description",
                "Lorem Ipsum is simply dummy text of",
                "Random text",
                "text"
            } },
        };

        public static Dictionary<string, string[]> filterData = new Dictionary<string, string[]>{
            { "user", new string[] {
                "task_user_id-selectized",
                "Silviya",
                "table",
                "//*[@id='tasks-table']/tbody/tr[1]/td[4]"
            } },
            { "businesses", new string[] {
                "task_listing_id-selectized",
                "Metropole",
                "table",
                "//*[@id='tasks-table']/tbody/tr/td[3]",
            } },
            { "status", new string[] {
                "task_status_id-selectized",
                "In Process",
                "table",
                "//*[@id='tasks-table']/tbody/tr/td[7]/span",
            } },
            { "tags", new string[] {
                "task_tag_id-selectized",
                "Post content",
                "task",
                "",
            } },
            { "regions", new string[] {
                "task_region_id-selectized",
                "EUROPE",
                "listing",
                "//*[@id='reporting-gmb']/tbody/tr/td[3]",
            } },
            { "manager", new string[] {
                "#vue_object > div:nth-child(2) > div.md-card-content > div.uk-grid > div:nth-child(6) > div > div > div.selectize-input.items.not-full.has-options > input[type='text']",
                "Powell",
                "listing",
                "//*[@id='reporting-gmb']/tbody/tr/td[6]",
            } },
            { "srManager", new string[] {
                "sr_manager-selectized",
                "Andreas",
                "listing",
                "//*[@id='reporting-gmb']/tbody/tr/td[5]",
            } },
            { "director", new string[] {
                "director-selectized",
                "Oliver",
                "listing",
                "//*[@id='reporting-gmb']/tbody/tr/td[4]",
            } },
        };

        public static void NavigateToTasks(IWebDriver instance)
        {
            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Click on the project section in the sidebar");
            LocateElement(instance, By.XPath("//*[@id='project_tab']/a"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 35);
        }

        public static void CheckTaskDeadlineNotifications(IWebDriver instance)
        {
            string baseURL = ConfigurationManager.AppSettings["url"];
            List<string> tasks = new List<string>();

            LogAction("Click to go to the overdue tasks tab");
            LocateElement(instance, By.XPath("//*[@id='overdue']/a"), 10).Click();

            Thread.Sleep(1500);

            DateTime dateTime = DateTime.UtcNow.Date.AddDays(-1);
            //DateTime dateTime = DateTime.UtcNow.Date;

            LogAction("Filter the tasks by the current date");
            LocateElement(instance, By.XPath("//*[@id='tasks-table']/thead/tr[2]/th[6]/input"), 10).SendKeys(dateTime.ToString("dd-MM-yyyy"));

            IList<IWebElement> taskNames = instance.FindElements(By.ClassName("taskName"));

            Console.WriteLine(taskNames.Count());

            if(taskNames.Count() != 0)
            {
                LogAction("Go through every task name");
                foreach (var taskName in taskNames)
                {
                    tasks.Add(taskName.Text);
                }

                LogAction("Go to the email manager");
                instance.Navigate().GoToUrl(baseURL + "/admin/email-manager");

                Thread.Sleep(1500);

                foreach (string task in tasks)
                {
                    bool emailSent = false;
                    LogAction("Input task name in the search field");
                    LocateElement(instance, By.XPath("//*[@id='search']"), 10).Clear();
                    LocateElement(instance, By.XPath("//*[@id='search']"), 10).SendKeys(task);

                    LogAction("Click on the search button");
                    LocateElement(instance, By.XPath("/html/body/div/div[3]/div/form/button"), 10).Click();

                    Thread.Sleep(2500);

                    IList<IWebElement> subjects = instance.FindElements(By.XPath("/html/body/div/div[4]/div/table/tbody/tr/td[3]"));

                    LogAction("Go through every task name");
                    foreach (var subject in subjects)
                    {
                        Console.WriteLine(subject.Text);
                        emailSent = true;
                        break;
                    }

                    if (emailSent != true)
                    {
                        Assert.IsTrue(false);
                    }
                }
            } 
        }

        public static void TaskForeign(IWebDriver instance)
        {
            string baseURL = ConfigurationManager.AppSettings["url"];

            LogAction("Navigate to a single task page");
            instance.Navigate().GoToUrl(baseURL);

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 35);

            try
            {
                LogAction("Try to click save task button");
                LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();
            }
            catch
            {
                Console.WriteLine("Not allowed to save tasks");
            }

            LogAction("Click on the actions tab");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='task_actions_click']"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            try
            {
                LogAction("Try to click submit action button");
                LocateElement(instance, By.XPath("//*[@id='task_actions_tab']/div[1]/div[4]/a"), 10).Click();
            }
            catch
            {
                Console.WriteLine("Not allowed to submit actions");
            }

            LogAction("Click on the communication tab");
            IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='task_communication_click']"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement2);

            try
            {
                LogAction("Try to publish comment");
                LocateElement(instance, By.XPath("//*[@id='add_comment']"), 10).Click();
            }
            catch
            {
                Console.WriteLine("Not allowed to publish comment");
            }
        }

        public static void TaskEdit(IWebDriver instance, string taskName, bool canEdit = true, bool isModerator = false)
        {
            if(isModerator != true)
            {
                LogAction("Verify that we are on the tasks view");
                Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[3]/div[1]/h3"), 20).Text, Does.Contain("All Tasks"));

                LogAction("Filter to see test task");
                LocateElement(instance, By.XPath("//*[@id='tasks-table_filter']/div/input"), 20).SendKeys(taskName);

                LogAction("Click on first result");
                LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]"), 20).Click();
            }

            Thread.Sleep(2000);

            if (canEdit == true)
            {
                foreach (var attr in taskAttributes)
                {
                    if (attr.Value[3] == "text")
                    {
                        LogAction("Input " + attr.Key);
                        LocateElement(instance, By.Id(attr.Value[0]), 10).Clear();
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[2]);
                        if (attr.Key == "deadline")
                        {
                            LogAction("Close the calendar");
                            LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Escape);
                        }
                    }
                    else if (attr.Value[3] == "select")
                    {
                        LogAction("Filter " + attr.Key);
                        LocateElement(instance, By.XPath(attr.Value[4]), 10).Click();
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Backspace);
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[2]);

                        LogAction("Select the suggested " + attr.Key);
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Enter);
                    }
                    else
                    {
                        LogAction("Filter " + attr.Key);
                        LocateElement(instance, By.Id(attr.Value[0]), 10).Click();
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[2]);

                        LogAction("Select the suggested " + attr.Key);
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Enter);

                        LogAction("Close the dropdown");
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Escape);
                    }
                }
            }
            else
            {
                LogAction("Test if the user can change the status to completed");
                LocateElement(instance, By.XPath("//*[@id='task_details_tab']/div/div/div[8]/div/div/div[1]/div"), 10).Click();
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys(Keys.Backspace);
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys("completed");

                LogAction("Select the suggested status");
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys(Keys.Enter);

                LogAction("Save the task");
                LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 20);

                LogAction("Check if the notification message is success");
                Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 10).Text, Does.Contain("There was an error while saving task"));

                LogAction("Close the error notification");
                LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();

                LogAction("Filter status to something that will save");
                LocateElement(instance, By.XPath("//*[@id='task_details_tab']/div/div/div[8]/div/div/div[1]/div"), 10).Click();
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys(Keys.Backspace);
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys("client");

                LogAction("Select the suggested status");
                LocateElement(instance, By.Id("task_details_status-selectized"), 10).SendKeys(Keys.Enter);

                LogAction("Save the task");
                LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();

                LogAction("Check if the notification message is success");
                Assert.That(LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div > div"), 20).Text, Does.Contain("Success"));

                LogAction("Close the error notification");
                LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 20).Click();

                LogAction("Click on first result");
                LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]"), 20).Click();
            }
            

           /* LogAction("Upload file");
            instance.FindElement(By.Id("fileUploadInputTask")).SendKeys(@"C:\123.txt");

            Thread.Sleep(5500);

            LogAction("Click on the copy task button");
            LocateElement(instance, By.Id("copyTaskLink"), 10).Click();

            LogAction("Click on the save task button");
            LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Close the green notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();*/
        }

        public static void TaskCreation(IWebDriver instance, string taskName)
        {
            LogAction("Verify that we are on the tasks view");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='vue_object']/div[3]/div[1]/h3"), 20).Text, Does.Contain("All Tasks"));

            LogAction("Click on the add task button");
            LocateElement(instance, By.XPath("//*[@id='vue_object']/div[4]/a"), 10).Click();

            Thread.Sleep(2000);

            foreach (var attr in taskAttributes)
            {
                if(attr.Value[3] == "text")
                {
                    if (attr.Key == "taskName")
                    {
                        LogAction("Close the dropdown");
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(taskName);
                    }

                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[1]);
                    if(attr.Key == "deadline")
                    {
                        LogAction("Close the dropdown");
                        LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Escape);
                    }
                } else if(attr.Value[3] == "select")
                {
                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[1]);
                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Enter);
                }
                else
                {
                    LogAction("Filter Businesses");
                    LocateElement(instance, By.Id(attr.Value[0]), 10).Click();
                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(attr.Value[1]);

                    LogAction("Select the suggested hotel");
                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Enter);

                    LogAction("Close the dropdown");
                    LocateElement(instance, By.Id(attr.Value[0]), 10).SendKeys(Keys.Escape);
                }
            }

            LogAction("Upload file");
            instance.FindElement(By.Id("fileUploadInputTask")).SendKeys(@"C:\123.txt");

            Thread.Sleep(5500);

            LogAction("Click on the copy task button");
            LocateElement(instance, By.Id("copyTaskLink"), 10).Click();

            LogAction("Click on the save task button");
            LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Close the green notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();
        }

        public static void TaskActions(IWebDriver instance)
        {
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='task_actions_click']"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Input action description");
            LocateElement(instance, By.Id("action_description"), 10).SendKeys("Random Descript");

            LogAction("Click on action date");
            LocateElement(instance, By.Id("action_date"), 10).SendKeys("2018-12-12");

            LogAction("Focus out of the action date field");
            LocateElement(instance, By.Id("action_description"), 10).Click();

            LogAction("Click on the action hour");
            LocateElement(instance, By.Id("action_hour"), 10).Click();
            LocateElement(instance, By.Id("action_hour"), 10).SendKeys(Keys.ArrowDown);
            LocateElement(instance, By.Id("action_hour"), 10).SendKeys(Keys.Enter);

            LogAction("Click to submit action");
            LocateElement(instance, By.XPath("//*[@id='task_actions_tab']/div[1]/div[4]/a"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Input new text for the description");
            LocateElement(instance, By.Name("link"), 10).SendKeys("random");

            /*LogAction("Click to save the changes");
            LocateElement(instance, By.CssSelector("#task_actions_tab > div:nth-child(2) > div > div > div > div.uk-width-medium-1-2.uk-grid-margin.uk-row-first > a"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Close the green notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();*/

            LogAction("Click the button complete action");
            LocateElement(instance, By.XPath("//a[contains(.,'Complete Action')]"), 10).Click();

            //*[@id="task_actions_tab"]/div[2]/div/div/div/div[4]/a

            Thread.Sleep(1500);

            LogAction("Confirm the completion of the action");
            LocateElement(instance, By.XPath("//button[contains(.,'Ok')]"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            /*LogAction("Click on the completed accordion");
            IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='task_actions_tab']/div[3]/h3"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement2);

            Thread.Sleep(1000);

            LogAction("Check if the action is there");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='actions_table']/tbody/tr/td[1]"), 20).Text, Does.Contain("Random Descript"));*/
        }

        public static void TaskCommunication(IWebDriver instance)
        {
            LogAction("Click to go to the communication tab");
            IWebElement webElement3 = LocateElement(instance, By.XPath("//*[@id='task_communication_click']"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement3);

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            /*LogAction("attach a file");
            LocateElement(instance, By.XPath("//*[@id='taskCommentAttachment']"), 10).SendKeys(@"C:\123.txt");*/

            LogAction("Write a comment");
            LocateElement(instance, By.XPath("//*[@id='newTaskComment']"), 10).SendKeys("asdjfihfoudajskvfiou");

            Thread.Sleep(1500);

            LogAction("Publish comment");
            LocateElement(instance, By.XPath("//*[@id='add_comment']"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            //LogAction("Verify that the comment we just published is visualized");
            //Assert.That(LocateElement(instance, By.XPath("//*[@id='task_communication_tab']/div[1]/ul/li/article/div[1]/p/p"), 20).Text, Does.Contain("asdjfihfoudajskvfiou"));
        }

        public static void CloseModal(IWebDriver instance)
        {
            LogAction("Click to close the modal");
            IWebElement webElement2 = LocateElement(instance, By.CssSelector("#task_modal"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement2);

            Thread.Sleep(2000);
        }

        public static void FilterCheck(IWebDriver instance)
        {
            foreach (var filter in filterData)
            {
                if(filter.Key == "manager")
                {
                    LogAction("Filter " + filter.Key);
                    LocateElement(instance, By.CssSelector(filter.Value[0]), 10).Click();
                    LocateElement(instance, By.CssSelector(filter.Value[0]), 10).SendKeys(filter.Value[1]);

                    LogAction("Select the suggested " + filter.Key);
                    LocateElement(instance, By.CssSelector(filter.Value[0]), 10).SendKeys(Keys.Enter);

                    LogAction("Close the dropdown");
                    LocateElement(instance, By.CssSelector(filter.Value[0]), 10).SendKeys(Keys.Escape);
                }
                else
                {
                    LogAction("Filter " + filter.Key);
                    LocateElement(instance, By.Id(filter.Value[0]), 10).Click();
                    LocateElement(instance, By.Id(filter.Value[0]), 10).SendKeys(filter.Value[1]);

                    LogAction("Select the suggested " + filter.Key);
                    LocateElement(instance, By.Id(filter.Value[0]), 10).SendKeys(Keys.Enter);

                    LogAction("Close the dropdown");
                    LocateElement(instance, By.Id(filter.Value[0]), 10).SendKeys(Keys.Escape);
                }

                LogAction("Unfocus the select");
                LocateElement(instance, By.XPath("//*[@id='vue_object']/div[2]/div[1]/h3"), 10).Click();

                LogAction("Click on the filter button");
                LocateElement(instance, By.XPath("//*[@id='vue_object']/div[2]/div[2]/div[2]/div[9]/button"), 10).Click();

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 35);

                LogAction("Close the filter notification");
                LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();

                switch (filter.Value[2])
                {
                    case "table":
                        LogAction("Check if the table cell contains the value we need");
                        Assert.That(LocateElement(instance, By.XPath(filter.Value[3]), 20).Text, Does.Contain(filter.Value[1]));
                        break;
                    case "task":
                        bool tagFound = false;

                        LogAction("Click on first result");
                        LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]"), 20).Click();

                        IWebElement div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(10) > div > div > div.selectize-input.items.not-full.has-options.has-items"));

                        LogAction("Get childs of div");
                        IList<IWebElement> rows_table = div.FindElements(By.ClassName("item"));

                        LogAction("Go through every child to check if the tag is there");
                        foreach (var item in rows_table)
                        {
                            if (filter.Value[1] == item.Text.ToString())
                            {
                                LogAction("Found the tag");
                                tagFound = true;
                            }
                        }

                        if(tagFound == false)
                        {
                            LogAction("Didnt found the tag, canceling the for");
                            Assert.IsTrue(false);
                        }

                        LogAction("Close the modal");
                        LocateElement(instance, By.XPath("//*[@id='close_task']"), 10).Click();

                        Thread.Sleep(1500);

                        break;
                    case "listing":
                        string hotelName = "";

                        LogAction("Click on first result");
                        LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]"), 20).Click();

                        LogAction("Copy the first hotel name");
                        hotelName = LocateElement(instance, By.XPath("//*[@id='task_details_tab']/div/div/div[3]/div/div/div[1]/div/a[1]"), 10).Text;

                        LogAction("Close the modal");
                        LocateElement(instance, By.XPath("//*[@id='close_task']"), 10).Click();

                        LogAction("Click on the reporting menu in the sidebar");
                        try
                        {
                            LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10).Click();
                        }
                        catch
                        {
                            IWebElement webElement3 = LocateElement(instance, By.XPath("//*[@id='reporting_tab']/a"), 10);
                            instance.ExecuteJavaScript("arguments[0].click();", webElement3);
                        }

                        LogAction("Click on the gmb reporting option in the sidebar");
                        try
                        {
                            LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10).Click();
                        }
                        catch
                        {
                            IWebElement webElement2 = LocateElement(instance, By.XPath("//*[@id='gmb_reporting_tab']/a"), 10);
                            instance.ExecuteJavaScript("arguments[0].click();", webElement2);
                        }

                        LogAction("wait for the loader to disappear");
                        WaitForLoader(instance, 30);

                        LogAction("Click to close the orange notification");
                        LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center > div"), 10).Click();

                        LogAction("Filter by hotel name");
                        LocateElement(instance, By.XPath("//*[@id='reporting-gmb']/thead/tr/th[1]/div/input"), 10).SendKeys(hotelName);

                        HelpFunctions.LogAction("Click on the select columns button");
                        IWebElement webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/button[4]"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);

                        HelpFunctions.LogAction("Click on the director option");
                        webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/ul/li[11]"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);

                        HelpFunctions.LogAction("Click on the senior manager option");
                        webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/ul/li[12]"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);

                        HelpFunctions.LogAction("Click on the manager option");
                        webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/ul/li[13]"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);

                        HelpFunctions.LogAction("Click on the select columns button to close the dropdown modal");
                        webElement = HelpFunctions.LocateElement(instance, By.XPath("//*[@id='reporting-gmb_wrapper']/div[1]/div[1]/div/button[4]"), 10);
                        instance.ExecuteJavaScript("arguments[0].click();", webElement);

                        Thread.Sleep(2000);

                        LogAction("Check if the cell matches our value");
                        Assert.That(LocateElement(instance, By.XPath(filter.Value[3]), 20).Text, Does.Contain(filter.Value[1]));

                        LogAction("Click on the project section in the sidebar");
                        LocateElement(instance, By.XPath("//*[@id='project_tab']/a"), 10).Click();

                        LogAction("wait for the loader to disappear");
                        WaitForLoader(instance, 35);

                        break;
                    default:
                        break;
                }

                LogAction("Click to clear filters");
                LocateElement(instance, By.XPath("//*[@id='vue_object']/div[2]/div[2]/div[2]/div[10]/a"), 10).Click();

                LogAction("wait for the loader to disappear");
                WaitForLoader(instance, 35);

                LogAction("Close the filter notification");
                LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();
            }
        }

        public static int NotificationsTest(IWebDriver instance)
        {
            List<string> recipients = new List<string>();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Filter tasks for luau business");
            LocateElement(instance, By.XPath("//*[@id='tasks-table']/thead/tr[2]/th[3]/input"), 10).SendKeys("luau");

            LogAction("Click on the first result");
            LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]/span"), 10).Click();

            LogAction("Get the assignees selectize");
            IWebElement div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(4) > div > div > div.selectize-input.items.not-full.has-options.has-items"));

            LogAction("Get items of selectize");
            IList<IWebElement> items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every assignee");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            LogAction("Get the PIL selectize");
            div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(5) > div > div > div.selectize-input.items.not-full.has-options.has-items"));

            LogAction("Get childs of div");
            items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every PIL");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            LogAction("Get the manager selectize");
            div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(11) > div > div > div.selectize-input.items.required.has-options.full.has-items"));

            LogAction("Get childs of div");
            items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every manager");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            List<string> users = recipients.Distinct().ToList();

            users.ForEach(i => Console.Write("{0}\t", i));

            return users.Count;
            /*List<string> recipients = new List<string>();

            LogAction("Filter to see test task");
            LocateElement(instance, By.XPath("//*[@id='tasks-table_filter']/div/input"), 20).SendKeys("Test Task");

            LogAction("Click on the first task");
            LocateElement(instance, By.XPath("//*[@id='tasks-table']/tbody/tr[1]/td[2]/span"), 10).Click();

            Thread.Sleep(2500);

            LogAction("Get the assignees selectize");
            IWebElement div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(4) > div > div > div.selectize-input.items.not-full.has-options.has-items"));

            LogAction("Get items of selectize");
            IList<IWebElement> items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every assignee");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            LogAction("Get the PIL selectize");
            div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(5) > div > div > div.selectize-input.items.not-full.has-options.has-items"));

            LogAction("Get childs of div");
            items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every PIL");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            LogAction("Get the manager selectize");
            div = instance.FindElement(By.CssSelector("#task_details_tab > div > div > div:nth-child(11) > div > div > div.selectize-input.items.required.has-options.full.has-items"));

            LogAction("Get childs of div");
            items = div.FindElements(By.ClassName("item"));

            LogAction("Go through every manager");
            foreach (var item in items)
            {
                recipients.Add(item.Text);
            }

            List<string> distinct = recipients.Distinct().ToList();

            TaskActions(instance);

            TaskCommunication(instance);

            LogAction("Click to go to details tab");
            LocateElement(instance, By.XPath("//*[@id='task_details_click']"), 10).Click();

            LogAction("Add Assignees");
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).Click();
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).SendKeys("hakka");

            LogAction("Select the suggested assignee");
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).SendKeys(Keys.Enter);

            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).Click();
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).SendKeys("manasiev");

            LogAction("Select the suggested assignee");
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Close the dropdown");
            LocateElement(instance, By.Id("task_details_assignee-selectized"), 10).SendKeys(Keys.Escape);

            LogAction("Add PILS");
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).Click();
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).SendKeys("evgeni");

            LogAction("Select the suggested PIL");
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).SendKeys(Keys.Enter);

            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).Click();
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).SendKeys("admin");

            LogAction("Select the suggested PIL");
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Close the dropdown");
            LocateElement(instance, By.Id("task_details_person_in_loop-selectized"), 10).SendKeys(Keys.Escape);

            LogAction("Filter manager");
            LocateElement(instance, By.XPath("//*[@id='task_details_tab']/div/div/div[11]/div/div/div[1]/div"), 10).Click();
            LocateElement(instance, By.Id("task_details_owner-selectized"), 10).SendKeys(Keys.Backspace);
            LocateElement(instance, By.Id("task_details_owner-selectized"), 10).SendKeys("bamov");

            LogAction("Select the suggested manager");
            LocateElement(instance, By.Id("task_details_owner-selectized"), 10).SendKeys(Keys.Enter);

            LogAction("Save the changes to the task");
            LocateElement(instance, By.XPath("//*[@id='save_task']"), 10).Click();

            LogAction("wait for the loader to disappear");
            WaitForLoader(instance, 20);

            LogAction("Close the green notification");
            LocateElement(instance, By.XPath("//a[contains(.,'Close')]"), 10).Click();

            LogAction("Go to mailtrap url");
            instance.Navigate().GoToUrl("https://mailtrap.io/");

            LogAction("Click on the mailbox");
            LocateElement(instance, By.XPath("//*[@id='main']/div/div[2]/div/div/div[2]/table/tbody/tr/td[1]/div[1]/strong/a"), 10).Click();

            Thread.Sleep(3500);

            LogAction("Get all titles");
            IList<IWebElement> titles = instance.FindElements(By.CssSelector("li.email > span.title"));

            LogAction("Go through every child to check if the tag is there");
            foreach (var title in titles)
            {
                Console.WriteLine(title.Text);
            }*/
        }
    }
}
