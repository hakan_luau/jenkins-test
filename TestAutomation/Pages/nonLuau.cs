﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using TestAutomation.Utilities;

namespace TestAutomation.Pages
{
    public class nonLuau : HelpFunctions
    {
        public static void mailtrapTruncate(IWebDriver instance)
        {
            LogAction("Go to mailtrap url");
            instance.Navigate().GoToUrl("https://mailtrap.io/");

            LogAction("Click on log in link");
            LocateElement(instance, By.LinkText("Log in"), 10).Click();

            LogAction("Clear email field");
            LocateElement(instance, By.Id("user_email"), 10).Clear();

            LogAction("Input data to email field");
            LocateElement(instance, By.Id("user_email"), 10).SendKeys("staging@luau.bg");

            LogAction("Clear password field");
            LocateElement(instance, By.Id("user_password"), 10).Clear();

            LogAction("Input data to password field");
            LocateElement(instance, By.Id("user_password"), 10).SendKeys("v5wCQ-~OU1y5ox_3~P");

            LogAction("Click on login button");
            LocateElement(instance, By.Name("commit"), 10).Click();

            LogAction("Click on the mailbox");
            LocateElement(instance, By.XPath("//*[@id='main']/div/div[2]/div/div/div[2]/table/tbody/tr/td[1]/div[1]/strong/a"), 10).Click();

            Thread.Sleep(2500);

            LogAction("Clear the inbox");
            LocateElement(instance, By.XPath("//*[@id='main']/div/div[1]/div/div[1]/div/div[1]/a[2]"), 10).Click();

            IAlert alert = instance.SwitchTo().Alert();
            alert.Accept();

            Thread.Sleep(1500);

            string baseURL = ConfigurationManager.AppSettings["url"];

            LogAction("Go to luausoft url");
            instance.Navigate().GoToUrl(baseURL);
        }

        public static void checkReviewGoogle(IWebDriver instance, string reviewer, string replyText)
        {
            LogAction("Go to the luau group google search");
            instance.Navigate().GoToUrl("https://www.google.bg/search?q=luau+group&oq=luau+group&aqs=chrome..69i57j69i61l3j0l2.2227j0j4&sourceid=chrome&ie=UTF-8");

            LogAction("Click on reviews");
            LocateElement(instance, By.XPath("//*[@id='rhs_block']/div/div[1]/div/div[1]/div[2]/div[2]/div/div[2]/div[2]/div/div/span[2]/span/a/span"), 10).Click();

            LogAction("Click on the filter");
            IWebElement webElement = LocateElement(instance, By.XPath("//*[@id='gsr']/g-lightbox/div[2]/div[3]/div/div/div/div[1]/div[3]/div[2]/g-dropdown-menu/g-popup/div[1]/g-dropdown-button/g-dropdown-menu-button-caption"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            LogAction("Click to filter by date");
            webElement = LocateElement(instance, By.XPath("//*[@id='lb']/div/g-menu/g-menu-item[2]"), 10);
            instance.ExecuteJavaScript("arguments[0].click();", webElement);

            Thread.Sleep(2000);

            LogAction("Check if reviewers name is the same");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='reviewSort']/div/div[3]/div[1]/div[1]/div[1]/a"), 10).Text, Does.Contain(reviewer));

            LogAction("Check if replyText is the same as the one we input");
            Assert.That(LocateElement(instance, By.XPath("//*[@id='reviewSort']/div/div[3]/div[1]/div[2]"), 10).Text, Does.Contain(replyText));
        }

        public static void checkTaskNotification(IWebDriver instance, int userCount)
        {
            LogAction("Go to mailtrap url");
            instance.Navigate().GoToUrl("https://mailtrap.io/");

            LogAction("Click on the mailbox");
            LocateElement(instance, By.XPath("/html/body/header/div/div[2]/div/div/a"), 10).Click();

            LogAction("Click on luausoft mailbox");
            LocateElement(instance, By.XPath("//*[@id='main']/div/div[2]/div/div/div[2]/table/tbody/tr/td[1]/div[1]/strong/a/span"), 10).Click();

            Thread.Sleep(3500);

            LogAction("Get all titles");
            IList<IWebElement> titles = instance.FindElements(By.CssSelector("li.email > span"));

            LogAction("Go through every child to check if the tag is there");
            foreach (var title in titles)
            {
                string titleCheck = title.Text;
                if (titleCheck.Contains("has a new comment"))
                {
                    title.Click();

                    IWebElement emails = LocateElement(instance, By.CssSelector("#main > div > div.detail_region.frame > div > div.phm.pvs.flex-container-col.flex-item > div.fixed_mail_info_box.mail-info > div.mail_info_box.row.mbs > div.col2of3.relative > p:nth-child(3) > span"), 10);

                    int usersFound = emails.Text.Split(',').Length;

                    if(usersFound != userCount)
                    {
                        Assert.IsTrue(false);
                    }
                }
            }
        }
    }
}
