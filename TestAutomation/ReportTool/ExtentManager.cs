﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports.Reporter.Configuration;
using NUnit.Framework;
using System;
using System.Reflection;

namespace TestAutomation.ReportTool
{
    public class ExtentManager
    {
        private static string testBrowser = TestContext.Parameters.Get("browser");

        private static readonly Lazy<ExtentReports> _lazy = new Lazy<ExtentReports>(() => new ExtentReports());

        public static ExtentReports Instance { get { return _lazy.Value; } }

        static ExtentManager()
        {
            String timeStamp = GetTimestamp(DateTime.Now);
            string path = Assembly.GetCallingAssembly().CodeBase;
            string finalpth = path.Substring(0, path.LastIndexOf("bin")) + "Reports\\report_" + timeStamp + "(" + testBrowser + " )" + ".html";
            string localpath = new Uri(finalpth).LocalPath;

            var htmlReporter = new ExtentHtmlReporter(localpath);
            htmlReporter.Configuration().ChartLocation = ChartLocation.Top;
            htmlReporter.Configuration().ChartVisibilityOnOpen = true;
            htmlReporter.Configuration().DocumentTitle = "Extent/NUnit Samples";
            htmlReporter.Configuration().ReportName = "Extent/NUnit Samples";
            htmlReporter.Configuration().Theme = Theme.Standard;

            Instance.AttachReporter(htmlReporter);
        }

        static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMdd hh-mm-ss");
        }
    }
}
