﻿using OpenQA.Selenium;
using System;

namespace TestAutomation.ReportTool
{
    public class TakeScreenshot
    {
        private static string testBrowser = System.Environment.GetEnvironmentVariable("browser");

        public static string Capture(IWebDriver driver, string screenShotName)
        {
            ITakesScreenshot ts = (ITakesScreenshot)driver;

            String timeStamp = GetTimestamp(DateTime.Now);
            Screenshot screenshot = ts.GetScreenshot();
            //browser must be attached to name
            string pth = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string finalpth = pth.Substring(0, pth.LastIndexOf("bin")) + "Reports\\Screenshots\\" + screenShotName + "_" + timeStamp + "(" + testBrowser + " )" + ".png";
            string localpath = new Uri(finalpth).LocalPath;

            screenshot.SaveAsFile(localpath, ScreenshotImageFormat.Png);

            return localpath;
        }

        static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMdd hh-mm-ss");
        }
    }
}
