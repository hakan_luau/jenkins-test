﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace TestAutomation
{
    public class DriverFactory
    {
        private static IWebDriver instance;

        public static IWebDriver getInstance()
        {
            return instance;
        }

        public static IWebDriver getDriver(string parameter)
        {
            switch (parameter)
            {
                case "ie":
                    InternetExplorerOptions caps = new InternetExplorerOptions();
                    caps.IgnoreZoomLevel = true;
                    caps.EnableNativeEvents = false;
                    caps.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
                    caps.EnablePersistentHover = true;
                    caps.EnsureCleanSession = true;
                    instance = new InternetExplorerDriver(caps);
                    break;
                case "firefox":
                    instance = new FirefoxDriver();
                    break;
                case "chrome":
                    instance = new ChromeDriver();
                    break;
            }
            return instance;
        }
    }
}

