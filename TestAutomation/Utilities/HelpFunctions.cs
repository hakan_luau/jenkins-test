﻿using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TestAutomation.ReportTool;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace TestAutomation.Utilities
{
    public class HelpFunctions
    {
        public static void LogAction(string actionDescription)
        {
            ExtentTestManager.GetTest().Log(Status.Pass, actionDescription);
        }

        public static IWebElement LocateElement(IWebDriver instance, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(instance, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.Until(ExpectedConditions.ElementToBeClickable(by));

                return wait.Until(ExpectedConditions.ElementToBeClickable(by));
            }
            return instance.FindElement(by);
        }

        public static void WaitForLoader(IWebDriver instance, int waitTime = 20)
        {
            var waitDisSecond = new WebDriverWait(instance, TimeSpan.FromSeconds(waitTime));
            waitDisSecond.Until(ExpectedConditions.InvisibilityOfElementLocated(By.Id("loaderWrapper")));
        }

        public static void WaitForNewsRoomLoader(IWebDriver instance, int waitTime = 20)
        {
            var waitDisSecond = new WebDriverWait(instance, TimeSpan.FromSeconds(waitTime));
            waitDisSecond.Until(ExpectedConditions.InvisibilityOfElementLocated(By.ClassName("md-preloader")));
        }

        public static void CloseNotification(IWebDriver instance)
        {
            try
            {
                LocateElement(instance, By.XPath("/html/body/div[6]/div/div/a"), 10).Click();
            }
            catch
            {
            }
        }

        public static void checkSelectizeOptions(IWebDriver instance, int hotelsCount)
        {
            LogAction("Get selectize options");
            IList<IWebElement> selectElements = instance.FindElements(By.TagName("option"));

            LogAction("Count the number of options");
            int selectizeCount = ((IList)selectElements).Count;

            LogAction("Check if the number of options matches the hardcoded one");
            if (hotelsCount != selectizeCount)
            {
                Console.WriteLine("Number of options shown: " + selectizeCount);
                Console.WriteLine("Number of options expected: " + hotelsCount);
                Assert.Fail("Wrong number of options");
            }
        }

        public static bool IsElementClickable(IWebDriver instance, By by)
        {
            try
            {
                LocateElement(instance, by, 10).Click();
                return true;
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Could not find the element in the view");
                return false;
            }
        }
    }
}
