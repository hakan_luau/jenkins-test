﻿using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Configuration;
using System.Net;
using TestAutomation.ReportTool;

namespace TestAutomation
{
    [SetUpFixture]
    public class TestBase
    {
        public IWebDriver instance;
        private static string baseURL = ConfigurationManager.AppSettings["url"];

        [OneTimeSetUp]
        public void OneTime()
        {
            ExtentTestManager.CreateParentTest(GetType().Name);
        }

        public void Setup()
        {
            var parameter = "chrome";
            //var parameter = TestContext.Parameters.Get("browser");
            instance = DriverFactory.getDriver(parameter);
            instance.Manage().Window.Maximize();
            instance.Navigate().GoToUrl(baseURL);
            ExtentTestManager.CreateTest(TestContext.CurrentContext.Test.Name);
        }

        public void Cleanup(string testName, string message = "")
        {
            if (message != "")
            {
                string screenShotPath = TakeScreenshot.Capture(instance, testName);
                string finalPath = screenShotPath.Substring(screenShotPath.IndexOf(@"Screenshots"));
                ExtentTestManager.GetTest().Log(Status.Fail, "FAILURE: \r\n\r\n" + message + ExtentTestManager.GetTest().AddScreenCaptureFromPath(finalPath));
            }
            else
            {
                ExtentTestManager.GetTest().Log(Status.Pass, "Test ended with Success");
            }
            //instance.Quit();
        }

        [OneTimeTearDown]
        public void GenerateReport()
        {
            ExtentManager.Instance.Flush();
        }

        protected void UITest(Action action, String testName)
        {
            try
            {
                action();
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine("ArgumentNullException");
                Cleanup(testName, ex.Message);
                throw;
            }
            catch (WebException ex)
            {
                Console.WriteLine("WebException");
                Cleanup(testName, ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception");
                Cleanup(testName, ex.Message);
                throw;
            }
            Cleanup(testName);
        }
    }
}


