﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateAdminTest : TestBase
    {
        [Test]
        public void CreateAdmin()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["adminEmail"];

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Admin" },
                    { "lname", "Admin" },
                    { "type", "LG" },
                    { "xpath", "//*[@id='roles']/p[1]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
