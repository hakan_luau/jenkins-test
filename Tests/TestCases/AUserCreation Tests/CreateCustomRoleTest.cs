﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateCustomRoleTest : TestBase
    {
        [Test]
        public void CreateCustomRole()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["customRoleEmail"];

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Custom" },
                    { "lname", "Custom" },
                    { "type", "LG" },
                    { "xpath", "//*[@id='permissions']/div[1]/div/div/div/div/p/label" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

