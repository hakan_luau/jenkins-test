﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateModeratorTest : TestBase
    {
        [Test]
        public void CreateModerator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["modEmail"];

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Moderator" },
                    { "lname", "Moderator" },
                    { "type", "LG" },
                    { "xpath", "//*[@id='roles']/p[2]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
