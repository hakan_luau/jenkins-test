﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateMultiEcommerceDirectorTest : TestBase
    {
        [Test]
        public void CreateMultiEcommerceDirector()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["ecommerceDirectorEmail"];

                bool isMultiAcc = true;

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Multi" },
                    { "lname", "EDirector" },
                    { "type", "Client" },
                    { "xpath", "//*[@id='roles']/p[5]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                Administration.GiveAccess(instance, isMultiAcc);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

