﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateMultiEcommerceManagerTest : TestBase
    {
        [Test]
        public void CreateMultiEcommerceManager()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["multiEcommerceManagerEmail"];

                bool isMultiAcc = true;

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Multi" },
                    { "lname", "EManager" },
                    { "type", "Client" },
                    { "xpath", "//*[@id='roles']/p[6]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");
 
                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                Administration.GiveAccess(instance, isMultiAcc);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
