﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateMultiHotelManagerTest : TestBase
    {
        [Test]
        public void CreateMultiHotelManager()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["multiHotelManagerEmail"];

                bool isMultiAcc = true;

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Multi" },
                    { "lname", "HManager" },
                    { "type", "Client" },
                    { "xpath", "//*[@id='roles']/p[7]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                Administration.GiveAccess(instance, isMultiAcc);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

