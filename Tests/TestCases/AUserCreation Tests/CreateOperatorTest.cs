﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateOperatorTest : TestBase
    {
        [Test]
        public void CreateOperator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                LoginPage.Login(instance, loginEmail);

                string emailCreate = ConfigurationManager.AppSettings["operatorEmail"];

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Operator" },
                    { "lname", "Operator" },
                    { "type", "LG" },
                    { "xpath", "//*[@id='roles']/p[4]/div" }
                };
                
                Administration.CreateUser(instance, dataArray);

                Administration.MakeTechnician(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
