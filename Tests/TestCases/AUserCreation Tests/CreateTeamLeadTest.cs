﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.AUserCreation_Tests
{
    [TestFixture]
    public class CreateTeamLeadTest : TestBase
    {
        [Test]
        public void CreateTeamLead()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string emailCreate = ConfigurationManager.AppSettings["teamleadEmail"];

                var dataArray = new Dictionary<string, string> {
                    { "email", emailCreate },
                    { "fname", "Team" },
                    { "lname", "Lead" },
                    { "type", "LG" },
                    { "xpath", "//*[@id='roles']/p[3]/div" }
                };

                Setup();

                HelpFunctions.LogAction("Get the email for login");
                
                LoginPage.Login(instance, loginEmail);

                Administration.CreateUser(instance, dataArray);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
