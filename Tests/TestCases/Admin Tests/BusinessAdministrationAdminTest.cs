﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Admin_Tests
{
    [TestFixture]
    public class BusinessAdministrationAdminTest : TestBase
    {
        [Test]
        public void BusinessAdministrationAdmin()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["adminEmail"];

                LoginPage.Login(instance, loginEmail);

                BusinessView.AdministrationTest(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

