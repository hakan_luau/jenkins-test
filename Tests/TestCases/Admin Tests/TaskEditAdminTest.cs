﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Admin_Tests
{
    [TestFixture]
    public class TaskEditAdminTest : TestBase
    {
        [Test]
        public void TaskEditAdmin()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.TaskEdit(instance, "AdminTask");

                Tasks.TaskActions(instance);

                Tasks.TaskCommunication(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}


