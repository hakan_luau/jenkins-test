﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Client_Tests
{
    [TestFixture]
    public class MultiHotelReviewClientTest : TestBase
    {
        [Test]
        public void MultiHotelReviewTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["multiHotelManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.HotelReview(instance, "Hotel Manager");

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
