﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Client_Tests
{
    [TestFixture]
    public class SingleHotelReviewClientTest : TestBase
    {
        [Test]
        public void SingleHotelReviewTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["singleHotelManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.HotelReview(instance, "Hotel Manager", true);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
