﻿using NUnit.Framework;
using System;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Client_Tests
{
    [TestFixture]
    public class SingleHotelUserDetailsClientTest : TestBase
    {
        [Test]
        public void SingleHotelUserDetailsTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["singleHotelManagerEmail"];

                HelpFunctions.LogAction("Get the hotel count");
                int hotelsCount = Int32.Parse(ConfigurationManager.AppSettings["singleHotelCount"]);

                LoginPage.Login(instance, loginEmail);

                ClientPages.UserDetails(instance, hotelsCount, loginEmail);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
