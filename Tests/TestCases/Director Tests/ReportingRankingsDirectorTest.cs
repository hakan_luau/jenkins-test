﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Director_Tests
{
    [TestFixture]
    public class ReportingRankingsDirectorTest : TestBase
    {
        [Test]
        public void ReportingRankingsDirector()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["ecommerceDirectorEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.ReportingRankings(instance);

                LoginPage.Logout(instance);

            }, testMethodName);

        }
    }
}

