﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Director_Tests
{
    [TestFixture]
    public class ReviewDirectorTest : TestBase
    {
        [Test]
        public void ReviewDirector()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["ecommerceDirectorEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.HotelReview(instance, "Director");

                LoginPage.Logout(instance);

            }, testMethodName);
        }   
    }
}
