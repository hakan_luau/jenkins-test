﻿using NUnit.Framework;
using System;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Director_Tests
{
    [TestFixture]
    public class UserDetailsDirectorTest : TestBase
    {
        [Test]
        public void UserDetailsDirector()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["ecommerceDirectorEmail"];

                HelpFunctions.LogAction("Get the hotel count");
                int hotelsCount = Int32.Parse(ConfigurationManager.AppSettings["multiHotelCount"]);

                LoginPage.Login(instance, loginEmail);

                ClientPages.UserDetails(instance, hotelsCount, loginEmail);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
