﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class CustomRolePermissionsTest : TestBase
    {
        [Test]
        public void CustomRolePermissions()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                LoginPage.Login(instance, loginEmail);

                LeftNavigation.TestSidebarLink(instance);

            }, testMethodName);
        }
    }
}


