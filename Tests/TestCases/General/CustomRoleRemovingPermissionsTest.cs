﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class CustomRoleRemovingPermissionsTest : TestBase
    {
        [Test]
        public void CustomRoleRemovingPermissions()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                LoginPage.Login(instance, loginEmail);

                /*ClientPages.CustomRoleIssueTest(instance);*/

                LeftNavigation.TestRemovingPermissions(instance);

            }, testMethodName);
        }
    }
}

