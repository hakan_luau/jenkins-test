﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Configuration;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class EditAccessNotificationsTest : TestBase
    {
        [Test]
        public void EditAccessNotifications()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string baseURL = ConfigurationManager.AppSettings["url"];

                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.TaskCreation(instance, "NotifTask");

                Tasks.CloseModal(instance);

                nonLuau.mailtrapTruncate(instance);

                HelpFunctions.LogAction("Go to main page");
                instance.Navigate().GoToUrl(baseURL);

                BusinessView.GivingViewAccess(instance, "luau");

                HelpFunctions.LogAction("click on Businesses");
                HelpFunctions.LocateElement(instance, By.XPath("//*[@id='businesses_tab']/a"), 10).Click();

                BusinessView.GivingEditAccess(instance, "luau");

                Tasks.NavigateToTasks(instance);

                HelpFunctions.LogAction("wait for the loader to disappear");
                HelpFunctions.WaitForLoader(instance, 20);

                int magicNumber = Tasks.NotificationsTest(instance);

                Thread.Sleep(2000);

                Tasks.TaskCommunication(instance);

                nonLuau.checkTaskNotification(instance, magicNumber+1);

            }, testMethodName);
        }
    }
}


