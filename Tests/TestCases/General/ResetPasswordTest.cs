﻿using NUnit.Framework;
using TestAutomation;
using TestAutomation.Pages;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class ResetPasswordTest : TestBase
    {
        [Test]
        public void ResetPassword()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                LoginPage.ResetPassword(instance);

            }, testMethodName);
        }
    }
}
