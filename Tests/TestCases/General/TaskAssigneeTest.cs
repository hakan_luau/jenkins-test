﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class TaskAssigneeTest : TestBase
    {
        [Test]
        public void TaskAssignee()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = "erhan@luaugroup.com";

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.TaskEdit(instance, "OperatorTask", false);

                Tasks.TaskActions(instance);

                Tasks.TaskCommunication(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}


