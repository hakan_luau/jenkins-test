﻿using NUnit.Framework;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.General
{
    [TestFixture]
    public class TaskManagerTest : TestBase
    {
        [Test]
        public void TaskManager()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = "pavlina@luaugroup.com";

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.TaskEdit(instance, "OperatorTask");

                Tasks.TaskActions(instance);

                Tasks.TaskCommunication(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

