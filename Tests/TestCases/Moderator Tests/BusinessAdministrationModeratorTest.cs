﻿using NUnit.Framework;
using OpenQA.Selenium;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Moderator_Tests
{
    [TestFixture]
    public class BusinessAdministrationModeratorTest : TestBase
    {
        [Test]
        public void BusinessAdministrationModerator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["modEmail"];

                LoginPage.Login(instance, loginEmail);

                BusinessView.AdministrationTest(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}

