﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Moderator_Tests
{
    [TestFixture]
    public class TaskEditModeratorTest : TestBase
    {
        [Test]
        public void TaskEditModerator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["modEmail"];

                LoginPage.Login(instance, loginEmail);

                string baseURL = ConfigurationManager.AppSettings["url"];

                HelpFunctions.LogAction("Navigate to a single task page");
                instance.Navigate().GoToUrl(baseURL);

                Tasks.TaskEdit(instance, "ModeratorTest", true, true);

                Tasks.TaskActions(instance);

                Tasks.TaskCommunication(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}


