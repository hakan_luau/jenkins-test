﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Moderator_Tests
{
    [TestFixture]
    public class TaskFilterModeratorTest : TestBase
    {
        [Test]
        public void TaskFilterModerator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["modEmail"];

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.FilterCheck(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}



