﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Operator_Tests
{
    [TestFixture]
    public class BusinessVenuesTest : TestBase
    {
        [Test]
        public void BusinessVenues()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["operatorEmail"];

                LoginPage.Login(instance, loginEmail);

                BusinessView.VenuesTest(instance);
                

            }, testMethodName);
        }
    }
}



