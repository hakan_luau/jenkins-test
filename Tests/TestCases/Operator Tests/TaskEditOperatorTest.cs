﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.Operator_Tests
{
    [TestFixture]
    public class TaskEditOperatorTest : TestBase
    {
        [Test]
        public void TaskEditOperator()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["operatorEmail"];

                LoginPage.Login(instance, loginEmail);

                Tasks.TaskForeign(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}


