﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Configuration;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.TableCheck_Tests
{
    [TestFixture]
    public class CitationsActionsTableTest : TestBase
    {
        [Test]
        public void CitationsActionsTable()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string tableURL = ConfigurationManager.AppSettings["url"] + "/citations/actions";

                string tableXPath = "//*[@id='scraper-table']";

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                HelpFunctions.LogAction("Go to the table url");
                instance.Navigate().GoToUrl(tableURL);

                HelpFunctions.LogAction("Wait for loader to disappear");
                HelpFunctions.WaitForLoader(instance);

                TableColumns.CitationsActions(instance);

                LeftNavigation.TestTableVisibility(instance, tableXPath);

            }, testMethodName);
        }
    }
}
