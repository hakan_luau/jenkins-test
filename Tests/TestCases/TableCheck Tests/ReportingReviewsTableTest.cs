﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Configuration;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.TableCheck_Tests
{
    [TestFixture]
    public class ReportingReviewsTableTest : TestBase
    {
        [Test]
        public void ReportingReviewsTable()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string tableURL = ConfigurationManager.AppSettings["url"] + "/reports/reviews";

                string tableXPath = "//*[@id='reporting-table']";

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                HelpFunctions.LogAction("Go to the table url");
                instance.Navigate().GoToUrl(tableURL);

                HelpFunctions.LogAction("Wait for loader to disappear");
                HelpFunctions.WaitForLoader(instance, 80);

                try
                {
                    HelpFunctions.LogAction("click to close the warning notification");
                    HelpFunctions.LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
                }
                catch
                {
                }

                TableColumns.ReportingReviews(instance);

                LeftNavigation.TestTableVisibility(instance, tableXPath);

            }, testMethodName);
        }
    }
}
