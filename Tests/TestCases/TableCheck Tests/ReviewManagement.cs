﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Configuration;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.TableCheck_Tests
{
    [TestFixture]
    public class ReviewManagementTableTest : TestBase
    {
        [Test]
        public void ReviewManagementTable()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string tableURL = ConfigurationManager.AppSettings["url"] + "/reviews";

                string tableXPath = "//*[@id='active-table']";

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                HelpFunctions.LogAction("Go to the table url");
                instance.Navigate().GoToUrl(tableURL);

                HelpFunctions.LogAction("Wait for loader to disappear");
                HelpFunctions.WaitForLoader(instance);

                try
                {
                    HelpFunctions.LogAction("click to close the warning notification");
                    HelpFunctions.LocateElement(instance, By.CssSelector("body > div.uk-notify.uk-notify-bottom-center"), 10).Click();
                }
                catch
                {
                }

                TableColumns.ReviewManagementMulti(instance);

                LeftNavigation.TestTableVisibility(instance, tableXPath);

            }, testMethodName);
        }
    }
}
