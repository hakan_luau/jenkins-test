﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;
using System.Configuration;
using System.Threading;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.TableCheck_Tests
{
    [TestFixture]
    public class TasksTableTest : TestBase
    {
        [Test]
        public void TasksTable()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string tableURL = ConfigurationManager.AppSettings["url"] + "/tasks";

                string tableXPath = "//*[@id='tasks-table']";

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                HelpFunctions.LogAction("Go to the table url");
                instance.Navigate().GoToUrl(tableURL);

                HelpFunctions.LogAction("Wait for loader to disappear");
                HelpFunctions.WaitForLoader(instance);

                TableColumns.Tasks(instance);

                LeftNavigation.TestTableVisibility(instance, tableXPath);
            }, testMethodName);
        }
    }
}
