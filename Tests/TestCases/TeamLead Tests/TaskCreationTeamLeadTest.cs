﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.TeamLead_Tests
{
    [TestFixture]
    public class TaskCreationTeamLeadTest : TestBase
    {
        [Test]
        public void TaskCreationTeamLead()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                string loginTest = ConfigurationManager.AppSettings["teamLeadEmail"];

                LoginPage.Login(instance, loginEmail);

                Tasks.NavigateToTasks(instance);

                Tasks.TaskCreation(instance, "TeamLeadTask");

                Tasks.TaskActions(instance);

                Tasks.TaskCommunication(instance);

                Tasks.CloseModal(instance);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}


