﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using System;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    class MultiHotelReportingGMBManagerTest : TestBase
    {
        [Test]
        public void MultiHotelReportingGMBTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["multiEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.ReportingGMB(instance);

                LoginPage.Logout(instance);

            }, testMethodName);

        }
    }
}