﻿using NUnit.Framework;
using System;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    public class MultiHotelReportingRankingsManagerTest : TestBase
    {
        [Test]
        public void MultiHotelReportingRankingsManager()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["multiEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                int hotelsCount = Int32.Parse(ConfigurationManager.AppSettings["multiHotelCount"]);

                ClientPages.ReportingRankings(instance);

                LoginPage.Logout(instance);

            }, testMethodName);

        }
    }
}

