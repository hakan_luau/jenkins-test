﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    public class MultiHotelReviewManagerTest : TestBase
    {
        [Test]
        public void MultiHotelReviewTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["multiEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.HotelReview(instance, "Ecommerce Manager");

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
