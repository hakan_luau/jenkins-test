﻿using NUnit.Framework;
using System;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    public class SingleHotelReportingGMBManagerTest : TestBase
    {
        [Test]
        public void SingleHotelReportingGMBTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["singleEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.ReportingGMB(instance, true);

                LoginPage.Logout(instance);

            }, testMethodName);

        }
    }
}
