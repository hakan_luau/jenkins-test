﻿using NUnit.Framework;
using System;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    public class SingleHotelReportingRankingsManagerTest : TestBase
    {
        [Test]
        public void SingleHotelReportingRankingsManager()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["singleEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.ReportingRankings(instance, true);

                LoginPage.Logout(instance);

            }, testMethodName);

        }
    }
}


