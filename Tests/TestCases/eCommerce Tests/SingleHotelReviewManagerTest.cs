﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.eCommerce_Tests
{
    [TestFixture]
    public class SingleHotelReviewManagerTest : TestBase
    {
        [Test]
        public void SingleHotelReviewTest()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                Setup();

                HelpFunctions.LogAction("Get the email for login");
                string loginEmail = ConfigurationManager.AppSettings["singleEcommerceManagerEmail"];

                LoginPage.Login(instance, loginEmail);

                ClientPages.HotelReview(instance, "Ecommerce Manager", true);

                LoginPage.Logout(instance);

            }, testMethodName);
        }
    }
}
