﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.zDeletionTests
{
    [TestFixture]
    public class DeleteOtherUsersTest : TestBase
    {
        [Test]
        public void DeleteOtherUsers()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["masterEmail"];

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                string[] emails = {
                    ConfigurationManager.AppSettings["adminEmail"],
                    ConfigurationManager.AppSettings["modEmail"],
                    ConfigurationManager.AppSettings["teamleadEmail"],
                    ConfigurationManager.AppSettings["operatorEmail"],
                    ConfigurationManager.AppSettings["ecommerceDirectorEmail"],
                    ConfigurationManager.AppSettings["singleEcommerceManagerEmail"],
                    ConfigurationManager.AppSettings["multiEcommerceManagerEmail"],
                    ConfigurationManager.AppSettings["singleHotelManagerEmail"],
                    ConfigurationManager.AppSettings["customRoleEmail"]
                };

                foreach (string email in emails)
                {
                    Administration.DeleteUser(instance, email);
                }
                
            }, testMethodName);
        }
    }
}
