﻿using NUnit.Framework;
using System.Configuration;
using TestAutomation;
using TestAutomation.Pages;
using TestAutomation.Utilities;

namespace Tests.TestCases.zDeletionTests
{
    [TestFixture]
    public class DeleteOwnAccountTest : TestBase
    {
        [Test]
        public void DeleteOwnAccount()
        {
            string testMethodName = TestContext.CurrentContext.Test.Name;
            UITest(() =>
            {
                string loginEmail = ConfigurationManager.AppSettings["multiHotelManagerEmail"];

                Setup();

                HelpFunctions.LogAction("Get the email for login");

                LoginPage.Login(instance, loginEmail);

                Administration.DeleteOwnAcc(instance);

            }, testMethodName);
        }
    }
}
